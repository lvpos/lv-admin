
angular.module('login.ctrl', [])

// Login
.controller('loginCtrl', ['$scope', '$mdSidenav', '$location', 'Projects', 'AuthenticationService', 'sessionService',
                          function($scope, $mdSidenav, $location, Projects, AuthenticationService, sessionService) {

    AuthenticationService.ClearCredentials();
    
    $scope.login = {};
    $scope.loginAct = function(login) {
        AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
        Projects.curlPost(Projects.zUrl, 'api/login', login).then(function(msg) {
            var line = msg.data;
            if(msg.status == 200)
            {
                angular.extend(line.data, {menu_id: 1})
                sessionService.set('account', JSON.stringify(line.data));
                sessionService.set('uid_acc', line.data.session_key);
                $location.path('dashboard');
            }
        })
    }
    
    $scope.reg = {};
    $scope.regAct = function(reg) {
        AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
        Projects.curlPost(Projects.zUrl, '', login).then(function(msg) {
            var line = msg.data;
            if(msg.status == 200)
            {
                $location.path('login');
            }
        })
    }
    
    $scope.forgot = {};
    $scope.forgotAct = function(forgot) {
        AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
        Projects.curlPost(Projects.zUrl, '', login).then(function(msg) {
            var line = msg.data;
            if(msg.status == 200)
            {
                $location.path('login');
            }
        })
    }

}])

// Mainmenu
.controller('mainCtrl', ['$scope', '$mdSidenav', '$location', 'Projects', 'AuthenticationService', 'sessionService',
                         function($scope, $mdSidenav, $location, Projects, AuthenticationService, sessionService) {
    
    var dtAcc = JSON.parse(sessionService.get('account'));
    angular.extend(dtAcc, {menu_id: 1})
    $scope.accLog = dtAcc;
    
    AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
    Projects.curlPost(Projects.zUrl, 'api/home', dtAcc).then(function(msg) {
        var line = msg.data;
        if(msg.status == 200)
        {
            //console.log(line.data);
            sessionService.set('roles', JSON.stringify(line.data.menus));
        }
    })
    
    AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
    Projects.curlPost(Projects.zUrl, 'api/outlets/index', dtAcc).then(function(msg) {
        var line = msg.data;
        if(msg.status == 200)
        {
            //console.log(line.data);
            $scope.outlets = line.data;
        }
    })
    
    // Check Roles
    var paths = $location.path().split("/");
    var xpath = (paths[1] == 'dashboard') ? 'home' : paths[1];
    var ypath = (paths[2] != undefined) ? paths[2] : '';
    var roles = JSON.parse(sessionService.get('roles'));
    angular.forEach(roles, function(val) {
        if (val.menu_url == xpath || val.menu_url == ypath) {
            console.log(val);
            angular.extend(dtAcc, {menu_id: val.menu_id});
            sessionService.set('account', JSON.stringify(dtAcc));
        }
    })
    console.log(dtAcc);
    
}])

// Dashboards
.controller('dashCtrl', ['$scope', '$mdSidenav', '$location', 'Projects', 'AuthenticationService', 'sessionService', 'nmFormat', 'capitalize', '$sce',
                         function($scope, $mdSidenav, $location, Projects, AuthenticationService, sessionService, nmFormat, capitalize, $sce) {
    
    $scope.path = $location.path();
    $scope.tlmenu = 'Dashboard';
    $scope.mnmenu = {url1: 'templates/v1/menu.html', urlsd: 'templates/v1/sidebar/menu.html'};
    
    $scope.openSide = function(val) {
        $mdSidenav(val).toggle()
    }
    
    var dtAcc = JSON.parse(sessionService.get('account'));
    //angular.extend(dtAcc, {menu_id: 1})
    $scope.accLog = dtAcc; console.log(dtAcc);
    
    $scope.logout = function() {
        sessionService.destroy('uid_acc');
        sessionService.destroy('account');
        $location.path('login');
    }
    
    if ($scope.path == '/dashboard') {
        var d = new Date();
        var xmonth = parseFloat(d.getMonth())+1;
        var xdate = d.getFullYear()+'-'+xmonth+'-'+d.getDate();
        
        if (dtAcc.start_date == undefined && dtAcc.end_date == undefined) {
            angular.extend(dtAcc, {start_date: xdate, end_date: xdate});
            $scope.datePicker = {startDate: d.getTime(), endDate: d.getTime()};
        } else {
            var d1 = dtAcc.start_date.split("-");
            var x1 = d1[0]+'/'+d1[1]+'/'+d1[2];
            var d2 = dtAcc.end_date.split("-");
            var x2 = d2[0]+'/'+d2[1]+'/'+d2[2];
            $scope.datePicker = {startDate: new Date(x1).getTime(), endDate: new Date(x2).getTime()};
        }
        
        $scope.selOutlets = function(dt) {
            if (dt == undefined) {
                angular.extend(dtAcc, {outlet_id: '', outlet_data: ''});
            } else {
                angular.extend(dtAcc, {outlet_id: dt.outlet_id, outlet_data: dt});
            }
           
            sessionService.set('account', JSON.stringify(dtAcc));
            
            $scope.summary(dtAcc);
            $scope.dayofweek(dtAcc);
            $scope.hourly(dtAcc);
            $scope.cbvolume(dtAcc);
            $scope.cbsales(dtAcc);
            $scope.topitems(dtAcc);
            $scope.itemcat(dtAcc);
        }
        
        $scope.proDash = function(dt) {
            var d1 = new Date(dt.startDate);
            var m1 = d1.getMonth()+1;
            var x1 = d1.getFullYear()+'-'+m1+'-'+d1.getDate();
            
            var d2 = new Date(dt.endDate);
            var m2 = d2.getMonth()+1;
            var x2 = d2.getFullYear()+'-'+m2+'-'+d2.getDate();
            
            //console.log(x1+"/"+x2);
            angular.extend(dtAcc, {start_date: x1, end_date: x2});
            sessionService.set('account', JSON.stringify(dtAcc));
            
            $scope.summary(dtAcc);
            $scope.dayofweek(dtAcc);
            $scope.hourly(dtAcc);
            $scope.cbvolume(dtAcc);
            $scope.cbsales(dtAcc);
            $scope.topitems(dtAcc);
            $scope.itemcat(dtAcc);
        }
        
        $scope.ceilFormat = function(dt) {
            return $scope.valFormat(Math.ceil(dt));
        }
        
        $scope.valFormat = function(dt) {
            return nmFormat(1*dt);
        }
        
        // Sales Summary
        $scope.summary = function(dt) {
            console.log(dt);
            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
            Projects.curlPost(Projects.zUrl, 'api/report/sales_summary', dt).then(function(msg) {
                var line = msg.data;
                if(msg.status == 200)
                {
                    //console.log(line.data);
                    $scope.lnSummary = line.data[0];
                }
            })
        }
        $scope.summary(dtAcc);
        
        // Day of Week
        $scope.dayofweek = function(dt) {
            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
            Projects.curlPost(Projects.zUrl, 'api/report/day_of_week_gross_sales', dt).then(function(msg) {
                var line = msg.data;
                if(msg.status == 200)
                {
                    //console.log(line.data);
                    $scope.lndow = line.data;
                    
                    $scope.labels_dow = ["Sunday", "Monday", "Tuesday", "wednesday", "Thursday", "Friday", "Saturday"];
                    $scope.series_dow = ['Day of The Week'];
                    
                    $scope.data_dow = [];
                    $i = 0;
                    angular.forEach(line.data, function(val){
                        $scope.data_dow[$i] = val.gross_sales;
                        $i++;
                    })
                    $scope.data_dow = [
                      $scope.data_dow
                    ];
                    $scope.onClick_dow = function (points, evt) {
                      console.log(points, evt);
                    };
                    $scope.datasetOverride_dow = [{ yAxisID: 'y-axis' }];
                    $scope.options_dow = {
                      scales: {
                        yAxes: [
                          {
                            id: 'y-axis',
                            type: 'linear',
                            display: true,
                            position: 'left'
                          }
                        ]
                      }
                    };
                }
            })
        }
        $scope.dayofweek(dtAcc);
        
        // Hourly
        $scope.hourly = function(dt) {
            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
            Projects.curlPost(Projects.zUrl, 'api/report/hourly_gross_sales_amount', dt).then(function(msg) {
                var line = msg.data;
                if(msg.status == 200)
                {
                    //console.log(line.data);
                    $scope.lnHourly = line.data;
                    
                    $scope.labels_hourly = [];
                    $scope.series_hourly = ['Hourly'];
                    
                    $scope.data_hourly = [];
                    $i = 0;
                    angular.forEach(line.data, function(val){
                        $scope.labels_hourly[$i] = $i;
                        $scope.data_hourly[$i] = val.gross_sales;
                        $i++;
                    })
                    $scope.data_hourly = [
                      $scope.data_hourly
                    ];
                    $scope.onClick_hourly = function (points, evt) {
                      console.log(points, evt);
                    };
                    $scope.datasetOverride_hourly = [{ yAxisID: 'y-axis' }];
                    $scope.options_hourly = {
                      scales: {
                        yAxes: [
                          {
                            id: 'y-axis',
                            type: 'linear',
                            display: true,
                            position: 'left'
                          }
                        ]
                      }
                    };
                }
            })
        }
        $scope.hourly(dtAcc);
        
        // Category by Volume
        $scope.cbvolume = function(dt) {
            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
            Projects.curlPost(Projects.zUrl, 'api/report/sales_category_by_volume', dt).then(function(msg) {
                var line = msg.data;
                if(msg.status == 200)
                {
                    //console.log(line.data);
                    $scope.lnByvolume = line.data;
                    
                    $scope.labels_vol = [];
                    $scope.data_vol = [];
                    $i = 0;
                    angular.forEach(line.data, function(val){
                        if (val.category_name != null) {
                            $scope.labels_vol[$i] = capitalize(val.category_name);
                            $scope.data_vol[$i] = val.total;
                            $i++;
                        }
                    })
                    
                    $scope.options_vol = { legend: { display: true } };
                }
            })
        }
        $scope.cbvolume(dtAcc);
        
        // Category by Sales
        $scope.cbsales = function(dt) {
            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
            Projects.curlPost(Projects.zUrl, 'api/report/sales_category_by_sales', dt).then(function(msg) {
                var line = msg.data;
                if(msg.status == 200)
                {
                    //console.log(line.data);
                    $scope.lnBysales = line.data;
                    
                    $scope.labels_cbs = [];
                    $scope.data_cbs = [];
                    $i = 0;
                    angular.forEach(line.data, function(val){
                        if (val.category_name != null) {
                            $scope.labels_cbs[$i] = capitalize(val.category_name);
                            $scope.data_cbs[$i] = val.total;
                            $i++;
                        }
                    })
                    
                    $scope.options_cbs = { legend: { display: true } };
                }
            })
        }
        $scope.cbsales(dtAcc);
        
        // Top Items
        $scope.topitems = function(dt) {
            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
            Projects.curlPost(Projects.zUrl, 'api/report/top_items ', dt).then(function(msg) {
                var line = msg.data;
                if(msg.status == 200)
                {
                    //console.log(line.data);
                    $scope.lnTopitems = line.data;
                }
            })
        }
        $scope.topitems(dtAcc);
        
        // Top Items By Category
        $scope.itemcat = function(dt) {
            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
            Projects.curlPost(Projects.zUrl, 'api/report/top_items_by_category ', dt).then(function(msg) {
                var line = msg.data;
                if(msg.status == 200)
                {
                    //console.log(line);
                    $scope.lnItemcat = line;
                    
                    var log = [];
                    $x = 0;
                    angular.forEach(line, function(dt){
                        if (dt.category_name != undefined) {
                            var l1 = "ic"+$x;
                            var d1 = "ic"+$x;
                            
                            log[$x] = {category_name: dt.category_name, labels: l1, data: d1};
                            
                            $x++;
                        }
                    })
                    $scope.lnItemcat = log;
                    
                    var lab = [];
                    var dat = [];
                    $x = 0;
                    angular.forEach(line, function(dt){
                        if (dt.category_name != undefined) {
                            var l1 = "ic"+$x;
                            var d1 = "ic"+$x;
                            $x++;
                            
                            var labels = [];
                            var data = [];
                            $i = 0;
                            angular.forEach(dt.top_products, function(val){
                                labels[$i] = val.product_name.substring(0,6)+"..";
                                data[$i] = val.total;
                                $i++;
                            });
                            
                            lab[l1] = labels;
                            dat[d1] = data;
                        }
                    });
                    
                    $scope.labels_ic = lab;
                    $scope.data_ic = dat;
                    $scope.datasetOverride_ic = [
                                                    {
                                                        label: "Bar chart",
                                                        borderWidth: 1,
                                                        type: 'bar'
                                                    }
                                                ];
                }
            })
        }
        $scope.itemcat(dtAcc);
        
        $scope.toTrustedHTML = function(html) {
            return $sce.trustAsHtml(html);
        }
    }
}])

// Reports
.controller('reportsCtrl', ['$scope', '$mdSidenav', '$route', '$location', 'Projects', 'AuthenticationService', 'sessionService', '$mdMedia', '$mdDialog', 'nmFormat', 'opts', 'items',
                            function($scope, $mdSidenav, $route, $location, Projects, AuthenticationService, sessionService, $mdMedia, $mdDialog, nmFormat, opts, items) {
    
    $scope.path = $location.path();
    $scope.tlmenu = 'Reports';
    $scope.mnmenu = {url1: 'templates/v1/menu.html', url2: 'templates/v1/reports/menu.html', urlsd: 'templates/v1/sidebar/menu.html'};
    
    $scope.openSide = function(val) {
        $mdSidenav(val).toggle()
    }
    
    var dtAcc = JSON.parse(sessionService.get('account'));
    //angular.extend(dtAcc, {menu_id: 1})
    $scope.accLog = dtAcc;
    
    var d = new Date();
    var xmonth = parseFloat(d.getMonth())+1;
    var xdate = d.getFullYear()+'-'+xmonth+'-'+d.getDate();
    
    if (dtAcc.start_date == undefined && dtAcc.end_date == undefined) {
        angular.extend(dtAcc, {start_date: xdate, end_date: xdate});
        $scope.datePicker = {startDate: d.getTime(), endDate: d.getTime()};
    } else {
        var d1 = dtAcc.start_date.split("-");
        var x1 = d1[0]+'/'+d1[1]+'/'+d1[2];
        var d2 = dtAcc.end_date.split("-");
        var x2 = d2[0]+'/'+d2[1]+'/'+d2[2];
        $scope.datePicker = {startDate: new Date(x1).getTime(), endDate: new Date(x2).getTime()};
    }
    
    $scope.selOutlets = function(dt) {
        if (dt == undefined) {
            angular.extend(dtAcc, {outlet_id: '', outlet_data: ''});
        } else {
            angular.extend(dtAcc, {outlet_id: dt.outlet_id, outlet_data: dt});
        }
       
        sessionService.set('account', JSON.stringify(dtAcc));
        
        switch ($scope.path) {
            case '/reports': $scope.summary(dtAcc); break;
            case '/reports/item-sales': $scope.trans(dtAcc); break;
            case '/reports/category-sales': $scope.cbsales(dtAcc); break;
            case '/reports/transaction': $scope.summary(dtAcc); $scope.trans(dtAcc); break;
        }
    }
    
    $scope.proDash = function(dt) {
        var d1 = new Date(dt.startDate);
        var m1 = d1.getMonth()+1;
        var x1 = d1.getFullYear()+'-'+m1+'-'+d1.getDate();
        
        var d2 = new Date(dt.endDate);
        var m2 = d2.getMonth()+1;
        var x2 = d2.getFullYear()+'-'+m2+'-'+d2.getDate();
        
        //console.log(x1+"/"+x2);
        angular.extend(dtAcc, {start_date: x1, end_date: x2});
        sessionService.set('account', JSON.stringify(dtAcc));
        
        switch ($scope.path) {
            case '/reports': $scope.summary(dtAcc); break;
            case '/reports/item-sales': $scope.trans(dtAcc); break;
            case '/reports/category-sales': $scope.cbsales(dtAcc); break;
            case '/reports/transaction': $scope.summary(dtAcc); $scope.trans(dtAcc); break;
        }
    }
    
    $scope.ceilFormat = function(dt) {
        return $scope.valFormat(Math.ceil(dt));
    }
    
    $scope.valFormat = function(dt) {
        return nmFormat(1*dt);
    }
    
    // Sales Summary
    if (opts == undefined) {
        $scope.summary = function(dt) {
            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
            Projects.curlPost(Projects.zUrl, 'api/report/sales_summary', dt).then(function(msg) {
                var line = msg.data;
                if(msg.status == 200)
                {
                    setTimeout(function() {
                        $scope.ls = line.data[0];
                    }, 100);
                }
            })
        }
    }
    
    if ($scope.path == '/reports') {
        $scope.type = 1;
        $scope.mainCT = 'templates/v1/reports/main/sub/summary.html';
        $scope.summary(dtAcc);
    }
    
    if ($scope.path == '/reports/item-sales') {
        $scope.type = 1;
        $scope.mainCT = 'templates/v1/reports/main/sub/item-sales.html';
        
        // Transaction
        $scope.trans = function(dt) {
            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
            Projects.curlPost(Projects.zUrl, 'api/sales/index/1000', dt).then(function(msg) {
                var line = msg.data;
                if(msg.status == 200)
                {
                    console.log(line.data);
                    
                    var log = [];
                    var i = 0;
                    angular.forEach(line.data, function(v1) {
                        angular.forEach(v1.sales_receipt_items, function(v2) {
                            var status = 0;
                            var x = 0;
                            angular.forEach(log, function(v3) {
                                if(v3.product_id == v2.product_id && v3.product_sales_price == v2.product_sales_price)
                                {
                                    var qty = parseFloat(v3.qty)+parseFloat(v2.qty);
                                    angular.extend(log[x], {qty: qty, gross_sales: parseFloat(qty)*parseFloat(v2.product_sales_price)});
                                    status = 1; 
                                }
                                x++;
                            })
                            
                            if (status == 0) {
                                log[i] = v2;
                                angular.extend(log[i], {gross_sales: parseFloat(v2.qty)*parseFloat(v2.product_sales_price)});
                                delete v2.subtotal;
                                i++;
                            }
                        })
                    });
                    
                    setTimeout(function() {
                        //console.log(log);
                        $scope.ls = log;
                    }, 200);
                }
            })
        }
        $scope.trans(dtAcc);
    }
    
    if ($scope.path == '/reports/category-sales') {
        $scope.type = 1;
        $scope.mainCT = 'templates/v1/reports/main/sub/category-sales.html';
        
        // Category by Sales
        $scope.cbsales = function(dt) {
            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
            Projects.curlPost(Projects.zUrl, 'api/report/sales_category_by_sales', dt).then(function(msg) {
                var line = msg.data;
                if(msg.status == 200)
                {
                    setTimeout(function() {
                        //console.log(line.data);
                        $scope.ls = line.data;
                    }, 100);
                }
            })
        }
        $scope.cbsales(dtAcc);
    }
    
    if ($scope.path == '/reports/transaction') {
        $scope.type = 2;
        $scope.mainCT = 'templates/v1/reports/main/sub/transaction.html';
        
        // Transaction
        if (opts == undefined) {
            $scope.summary(dtAcc);
            $scope.trans = function(dt) {
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/sales/index', dt).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        console.log(line.data);
                        $scope.listdata = line.data;
                    }
                })
            }
            $scope.trans(dtAcc);
        }
        
        // View Data
        if (opts != undefined && opts.type > 0) {
            angular.forEach(opts.data, function(val){
                if (val.sales_receipt_no == items) {
                    sessionService.set('cart', JSON.stringify(val));
                    $scope.dt = val;
                }
            })
            
            if (opts.type > 1) {
                $scope.nm = opts.name;
            }
        }
        
        $scope.showDetail = function(ev, item) {
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'reportsCtrl',
                templateUrl: 'templates/v1/reports/main/formdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 1, data: $scope.listdata}, items: item},
                fullscreen: true
            })
            .then(function(act) {
                switch (act) {
                    case 'resend':
                        $scope.reaction(ev, item, 'Resend Receipt', 'templates/v1/reports/main/receipt/resend.html', 'api/customers/create');
                    break;
                    case 'refund':
                        $scope.reaction(ev, item, 'Issue Refund', 'templates/v1/reports/main/receipt/refund.html', 'api/sales/refund/'+item);
                    break;
                }
            });
        }
        
        // Sub Total
        $scope.xsubtotal = function(type) {
            var cart = JSON.parse(sessionService.get('cart'));
            var val = 0; console.log(cart);
            
            if (cart != null) {
                angular.forEach(cart.sales_receipt_items, function(values) {
                    if (values.product_type == 'discount') {
                        val = val - (values.product_sales_price*values.qty);
                    } else {
                        val = val + (values.product_sales_price*values.qty);
                    }
                })
                
                if (type == 2) {
                    if (cart.sales_receipt_disc_amount != undefined && cart.sales_receipt_disc_amount != 0) {
                        val = val - parseFloat(cart.sales_receipt_disc_amount);
                    }
                    
                    if (cart.sales_receipt_tax_amount != undefined && cart.sales_receipt_tax_amount != 0) {
                        val = val + parseFloat(cart.sales_receipt_tax_amount);
                    }
                } console.log(val);
                
                if (type == 3) {
                    if (cart.sales_receipt_disc_amount != undefined && cart.sales_receipt_disc_amount != 0) {
                        val = val - parseFloat(cart.sales_receipt_disc_amount);
                    }
                    
                    if (cart.sales_receipt_tax_amount != undefined && cart.sales_receipt_tax_amount != 0) {
                        val = val + parseFloat(cart.sales_receipt_tax_amount);
                    }
                    
                    if (cart.sales_receipt_charges_amount != undefined && cart.sales_receipt_charges_amount != 0) {
                        val = val + parseFloat(cart.sales_receipt_charges_amount);
                    }
                }
                
                if (type > 0) {
                    return val;
                } else {
                    return nmFormat(val);
                }
            }
        }
        $scope.subtotal = $scope.xsubtotal(0);
        
        // Grand Total
        $scope.total = function(type) {
            return nmFormat($scope.xsubtotal(type));
        }
        
        // Change
        $scope.change = function(paid) {
            var change = nmFormat(parseFloat(paid) - $scope.xsubtotal(1));
            
            if (change == '') {
                return 0;
            } else {
                return change;
            }
        }
        
        // Resend Receipt & Issue Refund
        $scope.reaction = function(ev, item, title, tempUrl, actUrl) {
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'reportsCtrl',
                templateUrl: tempUrl,
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 2, name: title, action: actUrl}, items: item},
                fullscreen: true
            })
            .then(function(act) {
                $mdDialog.hide();
                $scope.showDetail(null, item);
            });
        }
        
        // Answer Sub Dialog
        $scope.answer = function(act) {
            $mdDialog.hide(act);
        }
        
        // Save & Update Data
        $scope.proceed = function(pp) {
            if (opts != undefined && opts.action != '') {
                if (pp.receipt_email != undefined) {
                    angular.extend(dtAcc, {customer_data: {customer_email: pp.receipt_email}});
                }
                angular.extend(dtAcc, {data: pp});
                
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, opts.action, dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }
        }
    }
    
    $scope.exportdata = function() {
        var data = [];
        switch ($scope.path) {
            case '/reports': data = $scope.ls; $name = 'summary'; break;
            case '/reports/item-sales': data = $scope.ls; $name = 'item-sales'; break;
            case '/reports/category-sales': data = $scope.ls; $name = 'category-sales'; break;
            case '/reports/transaction':
                $name = 'transaction'; 
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/sales/index/1000', dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $x = 0;
                        angular.forEach(line.data, function(val){
                            $dt = '';
                            angular.forEach(val.sales_receipt_items, function(v){
                                if ($dt == '') {
                                    $dt = v.product_name;
                                } else {
                                    $dt+= ", "+v.product_name;
                                }
                            })
                            angular.extend(val, {sales_receipt_items: $dt});
                            angular.extend(val, {sales_receipt_person: val.sales_receipt_person.name});
                            data[$x] = val;
                            $x++;
                        })
                        
                        console.log(data);
                        alasql('SELECT * INTO XLSX("'+$name+'-'+xdate+'.xlsx",{headers:true}) FROM ?',[data]);
                    }
                })
            break;
        }
        
        if (data != '') {
           alasql('SELECT * INTO XLSX("'+$name+'-'+xdate+'.xlsx",{headers:true}) FROM ?',[data]);
        }
    }
    $scope.datetime = function(dt) {
        return new Date(dt).getTime();
    }
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    
}])

// Products
.controller('productsCtrl', ['$scope', '$mdSidenav', '$route', '$location', 'Projects', 'AuthenticationService', 'sessionService',
                             '$mdMedia', '$mdDialog', 'nmFormat', 'opts', 'items', '$filter', 'getID',
                             function($scope, $mdSidenav, $route, $location, Projects, AuthenticationService, sessionService,
                                      $mdMedia, $mdDialog, nmFormat, opts, items, $filter, getID) {
    
    $scope.pic = Projects.zImg;
    $scope.path = $location.path();
    $scope.tlmenu = 'Products';
    $scope.mnmenu = {url1: 'templates/v1/menu.html', url2: 'templates/v1/products/menu.html', urlsd: 'templates/v1/sidebar/menu.html'};
    
    $scope.openSide = function(val) {
        $mdSidenav(val).toggle()
    }
    
    var dtAcc = JSON.parse(sessionService.get('account'));
    //angular.extend(dtAcc, {menu_id: 1})
    $scope.accLog = dtAcc;
    
    $scope.selOutlets = function(dt) {
        if (dt == undefined) {
            angular.extend(dtAcc, {outlet_id: '', outlet_data: ''});
        } else {
            angular.extend(dtAcc, {outlet_id: dt.outlet_id, outlet_data: dt});
        }
       
        sessionService.set('account', JSON.stringify(dtAcc));
    }
    
    $scope.valFormat = function(dt) {
        return nmFormat(1*dt);
    }
    
    $scope.sumQty = function(dt) {
        var x = 0;
        var log = ''
        angular.forEach(dt, function(r) {
            if (dtAcc.outlet_id == r.outlet_id) {
                log = r.actual_qty;
            } else {
                if (dtAcc.outlet_id == '') {
                    log = (x > 0) ? parseFloat(log)+parseFloat(r.actual_qty) : r.actual_qty;
                    x++;
                }
            }
        })
        
        return log;
    }
    
    $scope.delPic = function() {
        var files = $scope.myFile;
        
        if (files != undefined) {
            delete $scope.myFile;
            $('.md-subhead').html('No Image');
        } else {
            if ($scope.pp.product_info == undefined) {
                angular.extend($scope.pp, {product_info: {image_url: undefined}});
            } else {
                angular.extend($scope.pp.product_info, {image_url: undefined});
            }
        }
    }
    
    if ($scope.path == '/products') {
        $scope.sortCat = function(category) {
            console.log(category);
        }
        
        $scope.sortInv = function(inventories) {
            console.log(inventories);
        }
        
        if (items == undefined) {
            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
            Projects.curlPost(Projects.zUrl, 'api/products/index', dtAcc).then(function(msg) {
                var line = msg.data;
                if(msg.status == 200)
                {
                    $scope.listdata = line.data;
                    $scope.listcount = line.data.length;
                }
            })
        }
        
        if (opts != undefined) {
            $scope.prodType = {0: 'item', 1: 'inventory', 2: 'charge', 3: 'discount', 4: 'service'};
            if (items.error == undefined) {
                // Category
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/categories/index', dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        var log = [];
                        $x = 0;
                        angular.forEach(line.data, function(val) {
                            log[$x] = angular.extend(val, {selected: false});
                            $x++;
                        })
                        
                        $scope.catData = log;
                    }
                })
                
                // Outlets
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/outlets/index', dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        var log = [];
                        $x = 0;
                        angular.forEach(line.data, function(val) {
                            log[$x] = angular.extend(val, {selected: false});
                            $x++;
                        })
                        
                        $scope.outletData = log;
                    }
                })
            } else {
                $scope.catData = items.category_data;
                $scope.outletData = items.outlet_data;
            }
        }
        
        // Add Data
        if (opts != undefined && opts.type == 0) {
            $scope.nm = opts.name;
            $scope.pp = items; console.log($scope.pp);
        }
        
        $scope.showAdd = function(ev, item) {
            if (Object.prototype.toString.call(item) === '[object Object]') {
                $scope.pp = angular.extend(item.product_data, {error: 1});
            } else {
                $scope.pp = {is_active: 0};
            }
            
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'productsCtrl',
                templateUrl: 'templates/v1/products/main/formdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 0, name: 'Add Item', action: 'api/products/create'}, items: $scope.pp},
                fullscreen: true
            })
        }
        
        // Edit Data
        if (opts != undefined && opts.type > 0) {
            $scope.nm = opts.name;
            if (items.error == undefined) {
                $scope.showData = function(items) {
                    if (items > 0) {
                        AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                        Projects.curlPost(Projects.zUrl, 'api/products/show/'+items, dtAcc).then(function(msg) {
                            var line = msg.data;
                            if(msg.status == 200)
                            {
                                angular.extend(line.data, {product_basic_price: line.data.product_basic_price*1,
                                                           product_sales_price: line.data.product_sales_price*1,
                                                           product_qty: $scope.sumQty(line.data.product_actual_qty),
                                                           is_active: parseInt(line.data.is_active)});
                                $scope.pp = line.data; console.log(line.data);
                            }
                        })
                    }            
                }
                $scope.showData(items);
            } else {
                $scope.pp = items;
            }
        }
        
        $scope.showEdit = function(ev, item) {
            var id = item;
            if (Object.prototype.toString.call(item) === '[object Object]') {
                $scope.pp = angular.extend(item.product_data, {error: 1});
                id = $scope.pp.product_id;
                item = $scope.pp;
            }
            
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'productsCtrl',
                templateUrl: 'templates/v1/products/main/formdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 1, name: 'Edit Item', action: 'api/products/update/'+id}, items: item},
                fullscreen: true
            })
        }
        
        // Save & Update Data
        $scope.proceed = function(pp) {
            if (opts != undefined && opts.action != '') {
                var files = $scope.myFile; console.log(pp);
                var selCat = getID($filter('filter')($scope.catData, {selected: true}), 'category_id');
                var selOutlet = getID($filter('filter')($scope.outletData, {selected: true}), 'outlet_id');
                
                (pp.product_info == undefined) ? angular.extend(pp, {product_info: {empty: ''}}) : '';
                if (files != undefined) {
                    angular.extend(pp.product_info, {files: files});
                }
                
                angular.extend(pp, {category_data: $scope.catData, category_id: selCat, outlet_data: $scope.outletData, outlet_id: selOutlet});
                angular.extend(dtAcc, {product_data: pp, product_category_data: {category_id: selCat}, product_outlet_data: {outlet_id: selOutlet}});
                console.log(dtAcc);
                
                switch(opts.type) {
                    case 0: var popup = $scope.showAdd; break;
                    case 1: var popup = $scope.showEdit; break;
                }
                
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlMulti(Projects.zUrl, opts.action, dtAcc, popup).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }
        }
        
        // Delete
        $scope.delID = function(id) {
            if (id > 0) {
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/products/delete/'+id, dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }            
        }
        
        // Pro Action
        $scope.proAct = function(id, act) {
            if (id > 0) {
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/products/activator/'+id+'/'+act, dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }            
        }
    }
    
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    
}])

// Products -> Categories
.controller('categoriesCtrl', ['$scope', '$mdSidenav', '$route', '$location', 'Projects', 'AuthenticationService', 'sessionService',
                               '$mdMedia', '$mdDialog', 'opts', 'items', '$filter', 'getID',
                               function($scope, $mdSidenav, $route, $location, Projects, AuthenticationService, sessionService,
                                        $mdMedia, $mdDialog, opts, items, $filter, getID) {
    
    $scope.pic = Projects.zImg;
    $scope.path = $location.path();
    $scope.tlmenu = 'Categories';
    $scope.mnmenu = {url1: 'templates/v1/menu.html', url2: 'templates/v1/products/menu.html', urlsd: 'templates/v1/sidebar/menu.html'};
    
    $scope.openSide = function(val) {
        $mdSidenav(val).toggle()
    }
    
    var dtAcc = JSON.parse(sessionService.get('account'));
    //angular.extend(dtAcc, {menu_id: 1})
    $scope.accLog = dtAcc;
    
    $scope.delPic = function() {
        var files = $scope.myFile;
        
        if (files != undefined) {
            delete $scope.myFile;
            $('.md-subhead').html('No Image');
        } else {
            if ($scope.pp.category_info == undefined) {
                angular.extend($scope.pp, {category_info: {image_url: undefined}});
            } else {
                angular.extend($scope.pp.category_info, {image_url: undefined});
            }
        }
    }
    
    if ($scope.path == '/products/categories') {
        $scope.sortData = function(sorting) {
            console.log(sorting);
        }
        
        if (items == undefined) {
            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
            Projects.curlPost(Projects.zUrl, 'api/categories/index', dtAcc).then(function(msg) {
                var line = msg.data;
                if(msg.status == 200)
                {
                    $scope.listdata = line.data;
                    $scope.listcount = line.data.length;
                }
            })
        }
        
        if (opts != undefined) {
            if (items.error == undefined) {
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/products/index', dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        var log = [];
                        $x = 0;
                        angular.forEach(line.data, function(val) {
                            log[$x] = angular.extend(val, {selected: false});
                            $x++;
                        })
                        
                        $scope.prodData = log;
                    }
                })
            } else {
               $scope.prodData = items.product_data; 
            }
        }
        
        // Add Data
        if (opts != undefined && opts.type == 0) {
            $scope.nm = opts.name;
            $scope.pp = items; console.log($scope.pp);
        }
        
        $scope.showAdd = function(ev, item) {
            if (Object.prototype.toString.call(item) === '[object Object]') {
                $scope.pp = angular.extend(item.category_data, {error: 1});
            } else {
                $scope.pp = {is_active: 0};
            }
            
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'categoriesCtrl',
                templateUrl: 'templates/v1/products/categories/formdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 0, name: 'Add Category', action: 'api/categories/create'}, items: $scope.pp},
                fullscreen: true
            })
        }
        
        // Edit Data
        if (opts != undefined && opts.type > 0) {
            $scope.nm = opts.name;
            if (items.error == undefined) {
                $scope.showData = function(items) {
                    if (items > 0) {
                        AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                        Projects.curlPost(Projects.zUrl, 'api/categories/show/'+items, dtAcc).then(function(msg) {
                            var line = msg.data;
                            if(msg.status == 200)
                            {
                                angular.extend(line.data, {is_active: parseInt(line.data.is_active)});
                                $scope.pp = line.data;
                            }
                        })
                    }            
                }
                $scope.showData(items);
            } else {
                $scope.pp = items.category_data;
            }
        }
        
        $scope.showEdit = function(ev, item) {
            var id = item;
            if (Object.prototype.toString.call(item) === '[object Object]') {
                $scope.pp = angular.extend(item.category_data, {error: 1});
                id = $scope.pp.category_id;
                item = $scope.pp;
            }
            
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'categoriesCtrl',
                templateUrl: 'templates/v1/products/categories/formdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 1, name: 'Edit Category', action: 'api/categories/update/'+id}, items: item},
                fullscreen: true
            })
        }
        
        // Save & Update Data
        $scope.proceed = function(pp) {
            if (opts != undefined && opts.action != '') {
                var files = $scope.myFile;
                var selProd = getID($filter('filter')($scope.prodData, {selected: true}), 'product_id');
                
                (pp.category_info == undefined) ? angular.extend(pp, {category_info: {empty: ''}}) : '';
                if (files != undefined) {
                    angular.extend(pp.category_info, {files: files});
                }
                
                angular.extend(pp, {product_data: $scope.prodData, product_id: selProd});
                angular.extend(dtAcc, {category_data: pp, category_product_data: {product_id: selProd}});
                console.log(dtAcc);
                
                switch(opts.type) {
                    case 0: var popup = $scope.showAdd; break;
                    case 1: var popup = $scope.showEdit; break;
                }
                
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlMulti(Projects.zUrl, opts.action, dtAcc, popup).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }
        }
        
        // Delete
        $scope.delID = function(id) {
            if (id > 0) {
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/categories/delete/'+id, dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }            
        }
        
        // Pro Action
        $scope.proAct = function(id, act) {
            if (id > 0) {
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/categories/activator/'+id+'/'+act, dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }            
        }
    }
    
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    
}])

// Products -> Packages
.controller('packagesCtrl', ['$scope', '$mdSidenav', '$route', '$location', 'Projects', 'AuthenticationService', 'sessionService',
                             '$mdMedia', '$mdDialog', 'nmFormat', 'opts', 'items', '$filter', 'getID',
                             function($scope, $mdSidenav, $route, $location, Projects, AuthenticationService, sessionService,
                                      $mdMedia, $mdDialog, nmFormat, opts, items, $filter, getID) {
    
    $scope.pic = Projects.zImg;
    $scope.path = $location.path();
    $scope.tlmenu = 'Packages';
    $scope.mnmenu = {url1: 'templates/v1/menu.html', url2: 'templates/v1/products/menu.html', urlsd: 'templates/v1/sidebar/menu.html'};
    
    $scope.openSide = function(val) {
        $mdSidenav(val).toggle()
    }
    
    var dtAcc = JSON.parse(sessionService.get('account'));
    //angular.extend(dtAcc, {menu_id: 1})
    $scope.accLog = dtAcc;
    $scope.hidScan = true;
    
    $scope.valFormat = function(dt) {
        return nmFormat(1*dt);
    }
    
    $scope.delPic = function() {
        var files = $scope.myFile;
        
        if (files != undefined) {
            delete $scope.myFile;
            $('.md-subhead').html('No Image');
        } else {
            if ($scope.pp.package_info == undefined) {
                angular.extend($scope.pp, {package_info: {image_url: undefined}});
            } else {
                angular.extend($scope.pp.package_info, {image_url: undefined});
            }
        }
    }
    
    if ($scope.path == '/products/packages') {
        $scope.sortData = function(sorting) {
            console.log(sorting);
        }
        
        if (items == undefined) {
            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
            Projects.curlPost(Projects.zUrl, 'api/packages/index', dtAcc).then(function(msg) {
                var line = msg.data;
                if(msg.status == 200)
                {
                    $scope.listdata = line.data;
                    $scope.listcount = line.data.length;
                }
            })
        }
        
        if (opts != undefined) {
            // Package Products
            if (items.error == undefined) {
                if (opts != undefined && opts.type == 0) {
                    $scope.ppackData = [{product_id: '', qty: ''}];
                }
            } else {
                if (items.error > 0) {
                    $scope.ppackData = items.ppack_data;
                }
            }
            
            $scope.addPack = function(id) {
                var item = '';
                if (id == undefined) {
                    item = {product_id: '', qty: ''};
                } else {
                    angular.forEach($scope.prodData, function(val) {
                        if (val.product_code.toLowerCase() == id.toLowerCase()) {
                            item = {product_id: val.product_id, qty: ''};
                        }
                    })
                }
                
                if (Object.prototype.toString.call(item) === '[object Object]') {
                    if (id == undefined) {
                        $scope.ppackData.push(item); 
                    } else {
                        var xstats = 0;
                        if ($scope.ppackData.length == 1) {
                            if ($scope.ppackData[0].product_id == '') {
                                angular.extend($scope.ppackData[0], {product_id: item.product_id, qty: 1});
                            } else {
                                xstats = 1;
                            }
                        } else {
                            xstats = 1;
                        }
                        
                        if (xstats > 0) {
                            var x = 0;
                            var qty = 0;
                            var status = 0;
                            angular.forEach($scope.ppackData, function(val) {
                                if (val.product_id == item.product_id) {
                                    qty = parseFloat(val.qty)+1;
                                    angular.extend($scope.ppackData[x], {product_id: item.product_id, qty: qty});
                                    status = 1;
                                }
                                x++;
                            })
                            
                            if (status == 0) {
                                $scope.ppackData.push(item); 
                            }
                        }
                    }
                }
            }
            $scope.rmPack = function(item) { 
                var index = $scope.ppackData.indexOf(item);
                $scope.ppackData.splice(index, 1);     
            }
            
            if (items.error == undefined) {
                // Products
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/products/index', dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $scope.prodData = line.data;
                    }
                })
            } else {
                $scope.prodData = items.product_data;
            }
        }
        
        // Add Data
        if (opts != undefined && opts.type == 0) {
            $scope.nm = opts.name;
            $scope.pp = items; console.log($scope.pp);
        }
        
        $scope.showAdd = function(ev, item) {
            if (Object.prototype.toString.call(item) === '[object Object]') {
                $scope.pp = angular.extend(item.package_data, {error: 1});
            } else {
                $scope.pp = {is_active: 0};
            }
            
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'packagesCtrl',
                templateUrl: 'templates/v1/products/packages/formdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 0, name: 'Add Package', action: 'api/packages/create'}, items: $scope.pp},
                fullscreen: true
            })
        }
        
        // Edit Data
        if (opts != undefined && opts.type > 0) {
            $scope.nm = opts.name;
            if (items.error == undefined) {
                $scope.showData = function(items) {
                    if (items > 0) {
                        AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                        Projects.curlPost(Projects.zUrl, 'api/packages/show/'+items, dtAcc).then(function(msg) {
                            var line = msg.data;
                            if(msg.status == 200)
                            {
                                angular.extend(line.data, {package_price: line.data.package_price*1,
                                                           is_active: parseInt(line.data.is_active)});
                                $scope.pp = line.data;
                                
                                if (line.data.package_products == undefined) {
                                    $scope.ppackData = [{product_id: '', qty: ''}];
                                } else {
                                    $scope.ppackData = line.data.package_products;
                                }
                            }
                        })
                    }            
                }
                $scope.showData(items);
            } else {
                $scope.pp = items;
            }
        }
        
        $scope.showEdit = function(ev, item) {
            var id = item;
            if (Object.prototype.toString.call(item) === '[object Object]') {
                $scope.pp = angular.extend(item.package_data, {error: 1});
                id = $scope.pp.package_id;
                item = $scope.pp;
            }
            
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'packagesCtrl',
                templateUrl: 'templates/v1/products/packages/formdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 1, name: 'Edit Package', action: 'api/packages/update/'+id}, items: item},
                fullscreen: true
            })
        }
        
        // Save & Update Data
        $scope.proceed = function(pp) {
            if (opts != undefined && opts.action != '') {
                var files = $scope.myFile;
                
                //(pp.package_info == undefined) ? angular.extend(pp, {package_info: {empty: ''}}) : '';
                if (files != undefined) {
                    angular.extend(pp.package_pic_file, {files: files});
                }
                
                angular.extend(pp, {product_data: $scope.prodData, ppack_data: $scope.ppackData});
                angular.extend(dtAcc, {package_data: pp, package_products_data: $scope.ppackData});
                console.log(dtAcc);
                
                switch(opts.type) {
                    case 0: var popup = $scope.showAdd; break;
                    case 1: var popup = $scope.showEdit; break;
                }
                
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlMulti(Projects.zUrl, opts.action, dtAcc, popup).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }
        }
        
        // Delete
        $scope.delID = function(id) {
            if (id > 0) {
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/packages/delete/'+id, dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }            
        }
        
        // Pro Action
        $scope.proAct = function(id, act) {
            if (id > 0) {
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/packages/activator/'+id+'/'+act, dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }            
        }
    }
    
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    
}])

// Inventory In
.controller('inventinCtrl', ['$scope', '$mdSidenav', '$route', '$location', 'Projects', 'AuthenticationService', 'sessionService', '$mdMedia', '$mdDialog', 'opts', 'items',
                             function($scope, $mdSidenav, $route, $location, Projects, AuthenticationService, sessionService, $mdMedia, $mdDialog, opts, items) {
    
    $scope.path = $location.path();
    $scope.tlmenu = 'Inventory In';
    $scope.mnmenu = {url1: 'templates/v1/menu.html', url2: 'templates/v1/inventories/menu.html', urlsd: 'templates/v1/sidebar/menu.html'};
    
    $scope.openSide = function(val) {
        $mdSidenav(val).toggle()
    }
    
    var dtAcc = JSON.parse(sessionService.get('account'));
    //angular.extend(dtAcc, {menu_id: 1})
    $scope.accLog = dtAcc;
    $scope.hidScan = true;
    
    var d = new Date();
    var xmonth = parseFloat(d.getMonth())+1;
    var xdate = d.getFullYear()+'-'+xmonth+'-'+d.getDate();
    
    if (dtAcc.start_date == undefined && dtAcc.end_date == undefined) {
        angular.extend(dtAcc, {start_date: xdate, end_date: xdate});
        $scope.datePicker = {startDate: d.getTime(), endDate: d.getTime()};
    } else {
        var d1 = dtAcc.start_date.split("-");
        var x1 = d1[0]+'/'+d1[1]+'/'+d1[2];
        var d2 = dtAcc.end_date.split("-");
        var x2 = d2[0]+'/'+d2[1]+'/'+d2[2];
        $scope.datePicker = {startDate: new Date(x1).getTime(), endDate: new Date(x2).getTime()};
    }
    
    $scope.proDash = function(dt) {
        var d1 = new Date(dt.startDate);
        var m1 = d1.getMonth()+1;
        var x1 = d1.getFullYear()+'-'+m1+'-'+d1.getDate();
        
        var d2 = new Date(dt.endDate);
        var m2 = d2.getMonth()+1;
        var x2 = d2.getFullYear()+'-'+m2+'-'+d2.getDate();
        
        //console.log(x1+"/"+x2);
        angular.extend(dtAcc, {start_date: x1, end_date: x2});
        sessionService.set('account', JSON.stringify(dtAcc));
        
        $scope.invent(dtAcc);
    }
    
    $scope.invent = function(dt) {
        AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
        Projects.curlPost(Projects.zUrl, 'api/receive/index', dt).then(function(msg) {
            var line = msg.data;
            if(msg.status == 200)
            {
                $scope.listdata = line.data;
                $scope.listcount = line.data.length;
            }
        })
    }
    
    if ($scope.path == '/inventories/invent-in') {
        $scope.sortData = function(sorting) {
            console.log(sorting);
        }
        
        if (items == undefined) {
            $scope.invent(dtAcc);
        }
        
        if (opts != undefined) {
            if (opts.type < 2) {
                // Package Products
                if (items.error == undefined) {
                    if (opts != undefined && opts.type == 0) {
                        $scope.ppackData = [{product_id: '', qty: ''}];
                    }
                } else {
                    if (items.error > 0) {
                        $scope.ppackData = items.inventory_receipt_items;
                    }
                }
                
                $scope.addPack = function(id) {
                    var item = '';
                    if (id == undefined) {
                        item = {product_id: '', qty: ''};
                    } else {
                        angular.forEach($scope.prodData, function(val) {
                            if (val.product_code.toLowerCase() == id.toLowerCase()) {
                                item = {product_id: val.product_id, qty: ''};
                            }
                        })
                    }
                    
                    if (Object.prototype.toString.call(item) === '[object Object]') {
                        if (id == undefined) {
                            $scope.ppackData.push(item); 
                        } else {
                            var xstats = 0;
                            if ($scope.ppackData.length == 1) {
                                if ($scope.ppackData[0].product_id == '') {
                                    angular.extend($scope.ppackData[0], {product_id: item.product_id, qty: 1});
                                } else {
                                    xstats = 1;
                                }
                            } else {
                                xstats = 1;
                            }
                            
                            if (xstats > 0) {
                                var x = 0;
                                var qty = 0;
                                var status = 0;
                                angular.forEach($scope.ppackData, function(val) {
                                    if (val.product_id == item.product_id) {
                                        qty = parseFloat(val.qty)+1;
                                        angular.extend($scope.ppackData[x], {product_id: item.product_id, qty: qty});
                                        status = 1;
                                    }
                                    x++;
                                })
                                
                                if (status == 0) {
                                    $scope.ppackData.push(item); 
                                }
                            }
                        }
                    }
                }
                $scope.rmPack = function(item) { 
                    var index = $scope.ppackData.indexOf(item);
                    $scope.ppackData.splice(index, 1);     
                }
                
                if (items.error == undefined) {
                    // Locations
                    AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                    Projects.curlPost(Projects.zUrl, 'api/locations/index', dtAcc).then(function(msg) {
                        var line = msg.data;
                        if(msg.status == 200)
                        {
                            $scope.locsData = line.data;
                        }
                    })
                    
                    // Products
                    AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                    Projects.curlPost(Projects.zUrl, 'api/products/index', dtAcc).then(function(msg) {
                        var line = msg.data;
                        if(msg.status == 200)
                        {
                            $scope.prodData = line.data;
                        }
                    })
                } else {
                    $scope.locsData = items.location_data;
                    $scope.prodData = items.product_data;
                }
            }
        }
        
        // Add Data
        if (opts != undefined && opts.type == 0) {
            $scope.nm = opts.name;
            $scope.pp = items; console.log($scope.pp);
        }
        
        $scope.showAdd = function(ev, item) {
            if (Object.prototype.toString.call(item) === '[object Object]') {
                $scope.pp = angular.extend(item.inventory_data, {error: 1});
            } else {
                var d = new Date();
                $scope.pp = {is_active: 0, inventory_receipt_date: d};
            }
            
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'inventinCtrl',
                templateUrl: 'templates/v1/inventories/invent-in/formdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 0, name: 'Add Inventory In', action: 'api/receive/create'}, items: $scope.pp},
                fullscreen: true
            })
        }
        
        // Edit Data
        if (opts != undefined && opts.type > 0) {
            if (opts.type > 1) {
                $scope.pp = items;
            } else {
                $scope.nm = opts.name;
                if (items.error == undefined) {
                    $scope.showData = function(items) {
                        if (items > 0) {
                            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                            Projects.curlPost(Projects.zUrl, 'api/receive/show/'+items, dtAcc).then(function(msg) {
                                var line = msg.data;
                                if(msg.status == 200)
                                {
                                    angular.extend(line.data, {is_active: parseInt(line.data.is_active)});
                                    $scope.pp = line.data;
                                    
                                    //if (line.data.package_products == undefined) {
                                    //    $scope.ppackData = [{product_id: '', qty: ''}];
                                    //} else {
                                    //    $scope.ppackData = line.data.package_products;
                                    //}
                                }
                            })
                        }            
                    }
                    $scope.showData(items);
                } else {
                    $scope.pp = items;
                }
            }
        }
        
        $scope.showEdit = function(ev, item) {
            var id = item;
            if (Object.prototype.toString.call(item) === '[object Object]') {
                $scope.pp = angular.extend(item.inventory_data, {error: 1});
                id = $scope.pp.invent_id;
                item = $scope.pp;
            }
            
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'inventinCtrl',
                templateUrl: 'templates/v1/inventories/invent-in/formdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 1, name: 'Edit Inventory In', action: 'api/receive/update/'+id}, items: item},
                fullscreen: true
            })
        }
        
        $scope.showData = function(ev, item) {
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'inventinCtrl',
                templateUrl: 'templates/v1/inventories/invent-in/showdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 2, name: 'Show Data'}, items: item},
                fullscreen: true
            })
        }
        
        // Save & Update Data
        $scope.proceed = function(pp) {
            if (opts != undefined && opts.action != '') {
                var d = new Date(pp.inventory_receipt_date);
                var xmonth = parseFloat(d.getMonth())+1;
                var xdate = d.getFullYear()+'-'+xmonth+'-'+d.getDate();
                
                angular.extend(pp, {location_data: $scope.locsData, product_data: $scope.prodData, inventory_receipt_date: xdate,
                                    inventory_receipt_status: 'done', inventory_receipt_items: $scope.ppackData});
                angular.extend(dtAcc, {inventory_data: pp, inventory_receipt_items: $scope.ppackData, location_id: pp.location_id});
                console.log(dtAcc);
                
                switch(opts.type) {
                    case 0: var popup = $scope.showAdd; break;
                    case 1: var popup = $scope.showEdit; break;
                }
                
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, opts.action, dtAcc, popup).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }
        }
        
        // Delete
        $scope.delID = function(id) {
            if (id > 0) {
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/receive/delete/'+id, dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }            
        }
    }
    
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    
}])

// Inventory Out
.controller('inventoutCtrl', ['$scope', '$mdSidenav', '$route', '$location', 'Projects', 'AuthenticationService', 'sessionService', '$mdMedia', '$mdDialog', 'opts', 'items',
                             function($scope, $mdSidenav, $route, $location, Projects, AuthenticationService, sessionService, $mdMedia, $mdDialog, opts, items) {
    
    $scope.path = $location.path();
    $scope.tlmenu = 'Inventory Out';
    $scope.mnmenu = {url1: 'templates/v1/menu.html', url2: 'templates/v1/inventories/menu.html', urlsd: 'templates/v1/sidebar/menu.html'};
    
    $scope.openSide = function(val) {
        $mdSidenav(val).toggle()
    }
    
    var dtAcc = JSON.parse(sessionService.get('account'));
    //angular.extend(dtAcc, {menu_id: 1})
    $scope.accLog = dtAcc;
    $scope.hidScan = true;
    
    var d = new Date();
    var xmonth = parseFloat(d.getMonth())+1;
    var xdate = d.getFullYear()+'-'+xmonth+'-'+d.getDate();
    
    if (dtAcc.start_date == undefined && dtAcc.end_date == undefined) {
        angular.extend(dtAcc, {start_date: xdate, end_date: xdate});
        $scope.datePicker = {startDate: d.getTime(), endDate: d.getTime()};
    } else {
        var d1 = dtAcc.start_date.split("-");
        var x1 = d1[0]+'/'+d1[1]+'/'+d1[2];
        var d2 = dtAcc.end_date.split("-");
        var x2 = d2[0]+'/'+d2[1]+'/'+d2[2];
        $scope.datePicker = {startDate: new Date(x1).getTime(), endDate: new Date(x2).getTime()};
    }
    
    $scope.proDash = function(dt) {
        var d1 = new Date(dt.startDate);
        var m1 = d1.getMonth()+1;
        var x1 = d1.getFullYear()+'-'+m1+'-'+d1.getDate();
        
        var d2 = new Date(dt.endDate);
        var m2 = d2.getMonth()+1;
        var x2 = d2.getFullYear()+'-'+m2+'-'+d2.getDate();
        
        //console.log(x1+"/"+x2);
        angular.extend(dtAcc, {start_date: x1, end_date: x2});
        sessionService.set('account', JSON.stringify(dtAcc));
        
        $scope.invent(dtAcc);
    }
    
    $scope.invent = function(dt) {
        AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
        Projects.curlPost(Projects.zUrl, 'api/usage/index', dt).then(function(msg) {
            var line = msg.data;
            if(msg.status == 200)
            {
                $scope.listdata = line.data;
                $scope.listcount = line.data.length;
            }
        })
    }
    
    if ($scope.path == '/inventories/invent-out') {
        $scope.sortData = function(sorting) {
            console.log(sorting);
        }
        
        if (items == undefined) {
            $scope.invent(dtAcc);
        }
        
        if (opts != undefined) {
            if (opts.type < 2) {
                // Package Products
                if (items.error == undefined) {
                    if (opts != undefined && opts.type == 0) {
                        $scope.ppackData = [{product_id: '', qty: ''}];
                    }
                } else {
                    if (items.error > 0) {
                        $scope.ppackData = items.inventory_receipt_items;
                    }
                }
                
                $scope.addPack = function(id) {
                    var item = '';
                    if (id == undefined) {
                        item = {product_id: '', qty: ''};
                    } else {
                        angular.forEach($scope.prodData, function(val) {
                            if (val.product_code.toLowerCase() == id.toLowerCase()) {
                                item = {product_id: val.product_id, qty: ''};
                            }
                        })
                    }
                    
                    if (Object.prototype.toString.call(item) === '[object Object]') {
                        if (id == undefined) {
                            $scope.ppackData.push(item); 
                        } else {
                            var xstats = 0;
                            if ($scope.ppackData.length == 1) {
                                if ($scope.ppackData[0].product_id == '') {
                                    angular.extend($scope.ppackData[0], {product_id: item.product_id, qty: 1});
                                } else {
                                    xstats = 1;
                                }
                            } else {
                                xstats = 1;
                            }
                            
                            if (xstats > 0) {
                                var x = 0;
                                var qty = 0;
                                var status = 0;
                                angular.forEach($scope.ppackData, function(val) {
                                    if (val.product_id == item.product_id) {
                                        qty = parseFloat(val.qty)+1;
                                        angular.extend($scope.ppackData[x], {product_id: item.product_id, qty: qty});
                                        status = 1;
                                    }
                                    x++;
                                })
                                
                                if (status == 0) {
                                    $scope.ppackData.push(item); 
                                }
                            }
                        }
                    }
                }
                $scope.rmPack = function(item) { 
                    var index = $scope.ppackData.indexOf(item);
                    $scope.ppackData.splice(index, 1);     
                }
                
                if (items.error == undefined) {
                    // Locations
                    AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                    Projects.curlPost(Projects.zUrl, 'api/locations/index', dtAcc).then(function(msg) {
                        var line = msg.data;
                        if(msg.status == 200)
                        {
                            $scope.locsData = line.data;
                        }
                    })
                    
                    // Products
                    AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                    Projects.curlPost(Projects.zUrl, 'api/products/index', dtAcc).then(function(msg) {
                        var line = msg.data;
                        if(msg.status == 200)
                        {
                            $scope.prodData = line.data;
                        }
                    })
                } else {
                    $scope.locsData = items.location_data;
                    $scope.prodData = items.product_data;
                }
            }
        }
        
        // Add Data
        if (opts != undefined && opts.type == 0) {
            $scope.nm = opts.name;
            $scope.pp = items; console.log($scope.pp);
        }
        
        $scope.showAdd = function(ev, item) {
            if (Object.prototype.toString.call(item) === '[object Object]') {
                $scope.pp = angular.extend(item.inventory_data, {error: 1});
            } else {
                var d = new Date();
                $scope.pp = {is_active: 0, inventory_receipt_date: d};
            }
            
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'inventoutCtrl',
                templateUrl: 'templates/v1/inventories/invent-out/formdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 0, name: 'Add Inventory Out', action: 'api/usage/create'}, items: $scope.pp},
                fullscreen: true
            })
        }
        
        // Edit Data
        if (opts != undefined && opts.type > 0) {
            if (opts.type > 1) {
                $scope.pp = items; console.log($scope.pp);
            } else {
                $scope.nm = opts.name;
                if (items.error == undefined) {
                    $scope.showData = function(items) {
                        if (items > 0) {
                            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                            Projects.curlPost(Projects.zUrl, 'api/usage/show/'+items, dtAcc).then(function(msg) {
                                var line = msg.data;
                                if(msg.status == 200)
                                {
                                    angular.extend(line.data, {is_active: parseInt(line.data.is_active)});
                                    $scope.pp = line.data;
                                    
                                    //if (line.data.package_products == undefined) {
                                    //    $scope.ppackData = [{product_id: '', qty: ''}];
                                    //} else {
                                    //    $scope.ppackData = line.data.package_products;
                                    //}
                                }
                            })
                        }            
                    }
                    $scope.showData(items);
                } else {
                    $scope.pp = items;
                }
            }
        }
        
        $scope.showEdit = function(ev, item) {
            var id = item;
            if (Object.prototype.toString.call(item) === '[object Object]') {
                $scope.pp = angular.extend(item.inventory_data, {error: 1});
                id = $scope.pp.invent_id;
                item = $scope.pp;
            }
            
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'inventoutCtrl',
                templateUrl: 'templates/v1/inventories/invent-out/formdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 1, name: 'Edit Inventory Out', action: 'api/usage/update/'+id}, items: item},
                fullscreen: true
            })
        }
        
        $scope.showData = function(ev, item) {
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'inventoutCtrl',
                templateUrl: 'templates/v1/inventories/invent-out/showdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 2, name: 'Show Data'}, items: item},
                fullscreen: true
            })
        }
        
        // Save & Update Data
        $scope.proceed = function(pp) {
            if (opts != undefined && opts.action != '') {
                var d = new Date(pp.inventory_receipt_date);
                var xmonth = parseFloat(d.getMonth())+1;
                var xdate = d.getFullYear()+'-'+xmonth+'-'+d.getDate();
                
                angular.extend(pp, {location_data: $scope.locsData, product_data: $scope.prodData, inventory_receipt_date: xdate,
                                    inventory_receipt_status: 'done', inventory_receipt_items: $scope.ppackData});
                angular.extend(dtAcc, {inventory_data: pp, inventory_receipt_items: $scope.ppackData, location_id: pp.location_id});
                console.log(dtAcc);
                
                switch(opts.type) {
                    case 0: var popup = $scope.showAdd; break;
                    case 1: var popup = $scope.showEdit; break;
                }
                
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, opts.action, dtAcc, popup).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }
        }
        
        // Delete
        $scope.delID = function(id) {
            if (id > 0) {
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/usage/delete/'+id, dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }            
        }
    }
    
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    
}])

// Inventory Locations
.controller('inventlocsCtrl', ['$scope', '$mdSidenav', '$route', '$location', 'Projects', 'AuthenticationService', 'sessionService',
                               '$mdMedia', '$mdDialog', 'opts', 'items', '$filter', 'getID',
                               function($scope, $mdSidenav, $route, $location, Projects, AuthenticationService, sessionService,
                                        $mdMedia, $mdDialog, opts, items, $filter, getID) {
    
    $scope.pic = Projects.zImg;
    $scope.path = $location.path();
    $scope.tlmenu = 'Inventory Locations';
    $scope.mnmenu = {url1: 'templates/v1/menu.html', url2: 'templates/v1/inventories/menu.html', urlsd: 'templates/v1/sidebar/menu.html'};
    
    $scope.openSide = function(val) {
        $mdSidenav(val).toggle()
    }
    
    var dtAcc = JSON.parse(sessionService.get('account'));
    //angular.extend(dtAcc, {menu_id: 1})
    $scope.accLog = dtAcc;
    
    if ($scope.path == '/inventories/locations') {
        $scope.sortData = function(sorting) {
            console.log(sorting);
        }
        
        if (items == undefined) {
            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
            Projects.curlPost(Projects.zUrl, 'api/locations/index', dtAcc).then(function(msg) {
                var line = msg.data;
                if(msg.status == 200)
                {
                    $scope.listdata = line.data;
                    $scope.listcount = line.data.length;
                }
            })
        }
        
        // Add Data
        if (opts != undefined && opts.type == 0) {
            $scope.nm = opts.name;
            $scope.pp = items; console.log($scope.pp);
        }
        
        $scope.showAdd = function(ev, item) {
            if (Object.prototype.toString.call(item) === '[object Object]') {
                $scope.pp = angular.extend(item.location_data, {error: 1});
            } else {
                $scope.pp = {is_active: 0};
            }
            
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'inventlocsCtrl',
                templateUrl: 'templates/v1/inventories/locations/formdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 0, name: 'Add Location', action: 'api/locations/create'}, items: $scope.pp},
                fullscreen: true
            })
        }
        
        // Edit Data
        if (opts != undefined && opts.type > 0) {
            $scope.nm = opts.name;
            if (items.error == undefined) {
                $scope.showData = function(items) {
                    if (items > 0) {
                        AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                        Projects.curlPost(Projects.zUrl, 'api/locations/show/'+items, dtAcc).then(function(msg) {
                            var line = msg.data;
                            if(msg.status == 200)
                            {
                                $scope.pp = line.data;
                            }
                        })
                    }            
                }
                $scope.showData(items);
            } else {
                $scope.pp = items;
            }
        }
        
        $scope.showEdit = function(ev, item) {
            var id = item;
            if (Object.prototype.toString.call(item) === '[object Object]') {
                $scope.pp = angular.extend(item.location_data, {error: 1});
                id = $scope.pp.location_id;
                item = $scope.pp;
            }
            
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'inventlocsCtrl',
                templateUrl: 'templates/v1/inventories/locations/formdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 1, name: 'Edit Location', action: 'api/locations/update/'+id}, items: item},
                fullscreen: true
            })
        }
        
        // Save & Update Data
        $scope.proceed = function(pp) {
            if (opts != undefined && opts.action != '') {
                angular.extend(dtAcc, {location_data: pp});
                console.log(dtAcc);
                
                switch(opts.type) {
                    case 0: var popup = $scope.showAdd; break;
                    case 1: var popup = $scope.showEdit; break;
                }
                
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlMulti(Projects.zUrl, opts.action, dtAcc, popup).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }
        }
        
        // Delete
        $scope.delID = function(id) {
            if (id > 0) {
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/locations/delete/'+id, dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }            
        }
    }
    
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    
}])

// Customers
.controller('customersCtrl', ['$scope', '$mdSidenav', '$route', '$location', 'Projects', 'AuthenticationService', 'sessionService', '$mdMedia', '$mdDialog', 'opts', 'items',
                             function($scope, $mdSidenav, $route, $location, Projects, AuthenticationService, sessionService, $mdMedia, $mdDialog, opts, items) {
    
    $scope.path = $location.path();
    $scope.tlmenu = 'Customers';
    $scope.mnmenu = {url1: 'templates/v1/menu.html', url2: 'templates/v1/customers/menu.html', urlsd: 'templates/v1/sidebar/menu.html'};
    
    $scope.openSide = function(val) {
        $mdSidenav(val).toggle()
    }
    
    var dtAcc = JSON.parse(sessionService.get('account'));
    //angular.extend(dtAcc, {menu_id: 1})
    $scope.accLog = dtAcc;
    
    if ($scope.path == '/customers') {
        $scope.sortData = function(sorting) {
            console.log(sorting);
        }
        
        if (items == undefined) {
            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
            Projects.curlPost(Projects.zUrl, 'api/customers/index', dtAcc).then(function(msg) {
                var line = msg.data;
                if(msg.status == 200)
                {
                    $scope.listdata = line.data;
                    $scope.listcount = line.data.length;
                }
            })
        }
        
        // Add Data
        if (opts != undefined && opts.type == 0) {
            $scope.nm = opts.name;
            $scope.pp = items; console.log($scope.pp);
        }
        
        $scope.showAdd = function(ev, item) {
            if (Object.prototype.toString.call(item) === '[object Object]') {
                $scope.pp = angular.extend(item.customer_data, {error: 1});
            } else {
                $scope.pp = {is_active: 0};
            }
            
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'customersCtrl',
                templateUrl: 'templates/v1/customers/main/formdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 0, name: 'Add Customer', action: 'api/customers/create'}, items: $scope.pp},
                fullscreen: true
            })
        }
        
        // Edit Data
        if (opts != undefined && opts.type > 0) {
            $scope.nm = opts.name;
            if (items.error == undefined) {
                $scope.showData = function(items) {
                    if (items > 0) {
                        AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                        Projects.curlPost(Projects.zUrl, 'api/customers/show/'+items, dtAcc).then(function(msg) {
                            var line = msg.data;
                            if(msg.status == 200)
                            {
                                angular.extend(line.data, {is_active: parseInt(line.data.is_active)});
                                $scope.pp = line.data;
                            }
                        })
                    }            
                }
                $scope.showData(items);
            } else {
                $scope.pp = items;
            }
        }
        
        $scope.showEdit = function(ev, item) {
            var id = item;
            if (Object.prototype.toString.call(item) === '[object Object]') {
                $scope.pp = angular.extend(item.customer_data, {error: 1});
                id = $scope.pp.customer_id;
                item = $scope.pp;
            }
            
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'customersCtrl',
                templateUrl: 'templates/v1/customers/main/formdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 1, name: 'Edit Customer', action: 'api/customers/update/'+id}, items: item},
                fullscreen: true
            })
        }
        
        // Save & Update Data
        $scope.proceed = function(pp) {
            if (opts != undefined && opts.action != '') {
                angular.extend(dtAcc, {customer_data: pp});
                console.log(dtAcc);
                
                switch(opts.type) {
                    case 0: var popup = $scope.showAdd; break;
                    case 1: var popup = $scope.showEdit; break;
                }
                
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlMulti(Projects.zUrl, opts.action, dtAcc, popup).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }
        }
        
        // Delete
        $scope.delID = function(id) {
            if (id > 0) {
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/customers/delete/'+id, dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }            
        }
        
        // Pro Action
        $scope.proAct = function(id, act) {
            if (id > 0) {
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/customers/activator/'+id+'/'+act, dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }            
        }
    }
    
    $scope.custname = function(dt) {
        var nm = dt.split('@');
        xtract = nm[0].split('.');
        if (xtract.length == 1) {
            xtract = nm[0].split('_');
        }
        
        var fullname = xtract[0];
        if (xtract[1] != undefined) {
            fullname = xtract[0]+' '+xtract[1];
        }
        
        return fullname;
    }
    $scope.datetime = function(dt) {
        return new Date(dt).getTime();
    }
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    
}])

// User
.controller('userCtrl', ['$scope', '$mdSidenav', '$route', '$location', 'Projects', 'AuthenticationService', 'sessionService',
                         '$mdMedia', '$mdDialog', 'opts', 'items', '$filter', 'getID',
                         function($scope, $mdSidenav, $route, $location, Projects, AuthenticationService, sessionService,
                                  $mdMedia, $mdDialog, opts, items, $filter, getID) {
    
    $scope.pic = Projects.zImg;
    $scope.path = $location.path();
    $scope.tlmenu = 'User';
    $scope.mnmenu = {url1: 'templates/v1/menu.html', url2: 'templates/v1/user/menu.html', urlsd: 'templates/v1/sidebar/menu.html'};
    
    $scope.openSide = function(val) {
        $mdSidenav(val).toggle()
    }
    
    var dtAcc = JSON.parse(sessionService.get('account'));
    //angular.extend(dtAcc, {menu_id: 1})
    $scope.accLog = dtAcc;
    
    $scope.delPic = function() {
        var files = $scope.myFile;
        
        if (files != undefined) {
            delete $scope.myFile;
            $('.md-subhead').html('No Image');
        } else {
            if ($scope.pp.outlet_info == undefined) {
                angular.extend($scope.pp, {user_info: {image_url: undefined}});
            } else {
                angular.extend($scope.pp.user_info, {image_url: undefined});
            }
        }
    }
    
    if ($scope.path == '/user') {
        $scope.sortData = function(sorting) {
            console.log(sorting);
        }
        
        if (items == undefined) {
            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
            Projects.curlPost(Projects.zUrl, 'api/user/index', dtAcc).then(function(msg) {
                var line = msg.data;
                if(msg.status == 200)
                {
                    $scope.listdata = line.data;
                    $scope.listcount = line.data.length;
                }
            })
        }
        
        if (opts != undefined) {
            // Roles
            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
            Projects.curlPost(Projects.zUrl, 'api/roles/index', dtAcc).then(function(msg) {
                var line = msg.data;
                if(msg.status == 200)
                {
                    $scope.roleData = line.data;
                }
            })
        }
        
        if (opts != undefined) {
            if (items.error == undefined) {
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/outlets/index', dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        var log = [];
                        $x = 0;
                        angular.forEach(line.data, function(val) {
                            log[$x] = angular.extend(val, {selected: false});
                            $x++;
                        })
                        
                        $scope.outletData = log;
                    }
                })
            } else {
                $scope.outletData = items.outlet_data;
            }
        }
        // Add Data
        if (opts != undefined && opts.type == 0) {
            $scope.nm = opts.name;
            $scope.pp = items; console.log($scope.pp);
        }
        
        $scope.showAdd = function(ev, item) {
            if (Object.prototype.toString.call(item) === '[object Object]') {
                $scope.pp = angular.extend(item.user_data, {error: 1});
            } else {
                $scope.pp = {is_active: 0, role_id: ''};
            }
            
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'userCtrl',
                templateUrl: 'templates/v1/user/main/formdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 0, name: 'Add User', action: 'api/user/create'}, items: $scope.pp},
                fullscreen: true
            })
        }
        
        // Edit Data
        if (opts != undefined && opts.type > 0) {
            $scope.nm = opts.name;
            if (items.error == undefined) {
                $scope.showData = function(items) {
                    if (items > 0) {
                        AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                        Projects.curlPost(Projects.zUrl, 'api/user/show/'+items, dtAcc).then(function(msg) {
                            var line = msg.data;
                            if(msg.status == 200)
                            {
                                angular.extend(line.data, {is_active: parseInt(line.data.is_active)});
                                $scope.pp = line.data;
                            }
                        })
                    }            
                }
                $scope.showData(items);
            } else {
                $scope.pp = items;
            }
        }
        
        $scope.showEdit = function(ev, item) {
            var id = item;
            if (Object.prototype.toString.call(item) === '[object Object]') {
                $scope.pp = angular.extend(item.user_data, {error: 1});
                id = $scope.pp.user_id;
                item = $scope.pp;
            }
            
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'userCtrl',
                templateUrl: 'templates/v1/user/main/formdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 1, name: 'Edit User', action: 'api/user/update/'+id}, items: item},
                fullscreen: true
            })
        }
        
        // Save & Update Data
        $scope.proceed = function(pp) {
            if (opts != undefined && opts.action != '') {
                var files = $scope.myFile;
                var selOutlet = getID($filter('filter')($scope.outletData, {selected: true}), 'outlet_id');
                
                (pp.user_info == undefined) ? angular.extend(pp, {user_info: {empty: ''}}) : '';
                if (files != undefined) {
                    angular.extend(pp.user_info, {files: files});
                }
                
                angular.extend(pp, {outlet_data: $scope.outletData, outlet_id: selOutlet});
                angular.extend(dtAcc, {user_data: pp, user_outlet_data: {outlet_id: selOutlet}});
                console.log(dtAcc);
                
                switch(opts.type) {
                    case 0: var popup = $scope.showAdd; break;
                    case 1: var popup = $scope.showEdit; break;
                }
                
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlMulti(Projects.zUrl, opts.action, dtAcc, popup).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }
        }
        
        // Delete
        $scope.delID = function(id) {
            if (id > 0) {
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/user/delete/'+id, dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }            
        }
        
        // Pro Action
        $scope.proAct = function(id, act) {
            if (id > 0) {
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/user/activator/'+id+'/'+act, dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }            
        }
    }
    
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    
}])

// Roles
.controller('rolesCtrl', ['$scope', '$mdSidenav', '$route', '$location', 'Projects', 'AuthenticationService', 'sessionService', '$mdMedia', '$mdDialog', 'opts', 'items',
                          function($scope, $mdSidenav, $route, $location, Projects, AuthenticationService, sessionService, $mdMedia, $mdDialog, opts, items) {
    
    $scope.path = $location.path();
    $scope.tlmenu = 'Roles';
    $scope.mnmenu = {url1: 'templates/v1/menu.html', url2: 'templates/v1/user/menu.html', urlsd: 'templates/v1/sidebar/menu.html'};
    
    $scope.openSide = function(val) {
        $mdSidenav(val).toggle()
    }
    
    var dtAcc = JSON.parse(sessionService.get('account'));
    //angular.extend(dtAcc, {menu_id: 1})
    $scope.accLog = dtAcc;
    
    if ($scope.path == '/user/roles') {
        $scope.sortData = function(sorting) {
            console.log(sorting);
        }
        
        if (items == undefined) {
            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
            Projects.curlPost(Projects.zUrl, 'api/roles/index', dtAcc).then(function(msg) {
                var line = msg.data;
                if(msg.status == 200)
                {
                    $scope.listdata = line.data;
                    $scope.listcount = line.data.length;
                }
            })
        }
        
        // Add Data
        if (opts != undefined && opts.type == 0) {
            $scope.nm = opts.name;
            
            if (items.error == undefined) {
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/menu/index', dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        var log = [];
                        $x = 0;
                        angular.forEach(line.data, function(val) {
                            log[$x] = angular.extend(val, {enable_access: 0, enable_create: 0, enable_update: 0, enable_destroy: 0});
                            $x++;
                        })
                        
                        $scope.pp = {role_menus: log};
                    }
                })
            } else {
                $scope.pp = items;
            }
        }
        
        $scope.showAdd = function(ev, item) {
            if (Object.prototype.toString.call(item) === '[object Object]') {
                $scope.pp = angular.extend(item.role_data, {error: 1});
            } else {
                $scope.pp = {is_active: 0};
            }
            
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'rolesCtrl',
                templateUrl: 'templates/v1/user/roles/formdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 0, name: 'Add Roles', action: 'api/roles/create'}, items: $scope.pp},
                fullscreen: true
            })
        }
        
        // Edit Data
        if (opts != undefined && opts.type > 0) {
            $scope.nm = opts.name;
            if (items.error == undefined) {
                $scope.showData = function(items) {
                    if (items > 0) {
                        AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                        Projects.curlPost(Projects.zUrl, 'api/roles/show/'+items, dtAcc).then(function(msg) {
                            var line = msg.data;
                            if(msg.status == 200)
                            {
                                var log = [];
                                $x = 0;
                                angular.forEach(line.data.role_menus, function(val) {
                                    angular.extend(val, {enable_access: parseInt(val.enable_access), enable_create: parseInt(val.enable_create),
                                                         enable_update: parseInt(val.enable_update), enable_destroy: parseInt(val.enable_destroy)});
                                    log[$x] = val;
                                    
                                    $x++;
                                })
                                
                                $scope.pp = angular.extend(line.data, {role_menus: log});
                                console.log($scope.pp);
                            }
                        })
                    }            
                }
                $scope.showData(items);
            } else {
                $scope.pp = items;
            }
        }
        
        $scope.showEdit = function(ev, item) {
            var id = item;
            if (Object.prototype.toString.call(item) === '[object Object]') {
                $scope.pp = angular.extend(item.role_data, {error: 1});
                id = $scope.pp.role_id;
                item = $scope.pp;
            }
            
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'rolesCtrl',
                templateUrl: 'templates/v1/user/roles/formdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 1, name: 'Edit Roles', action: 'api/roles/update/'+id}, items: item},
                fullscreen: true
            })
        }
        
        // Save & Update Data
        $scope.proceed = function(pp) {
            if (opts != undefined && opts.action != '') {
                var menuData = pp.role_menus;
                angular.extend(dtAcc, {role_data: pp, role_menu_data: menuData});
                console.log(dtAcc);
                
                switch(opts.type) {
                    case 0: var popup = $scope.showAdd; break;
                    case 1: var popup = $scope.showEdit; break;
                }
                
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlMulti(Projects.zUrl, opts.action, dtAcc, popup).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }
        }
        
        // Delete
        $scope.delID = function(id) {
            if (id > 0) {
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/roles/delete/'+id, dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }            
        }
        
        // Pro Action
        $scope.proAct = function(id, act) {
            if (id > 0) {
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/roles/activator/'+id+'/'+act, dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }            
        }
    }
    
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    
}])

// Outlets
.controller('outletsCtrl', ['$scope', '$mdSidenav', '$route', '$location', 'Projects', 'AuthenticationService', 'sessionService',
                            '$mdMedia', '$mdDialog', 'opts', 'items', '$filter', 'getID',
                            function($scope, $mdSidenav, $route, $location, Projects, AuthenticationService, sessionService,
                                     $mdMedia, $mdDialog, opts, items, $filter, getID) {
    
    $scope.pic = Projects.zImg;
    $scope.path = $location.path();
    $scope.tlmenu = 'Outlets';
    $scope.mnmenu = {url1: 'templates/v1/menu.html', url2: 'templates/v1/outlets/menu.html', urlsd: 'templates/v1/sidebar/menu.html'};
    
    $scope.openSide = function(val) {
        $mdSidenav(val).toggle()
    }
    
    var dtAcc = JSON.parse(sessionService.get('account'));
    //angular.extend(dtAcc, {menu_id: 1})
    $scope.accLog = dtAcc;
    
    $scope.delPic = function() {
        var files = $scope.myFile;
        
        if (files != undefined) {
            delete $scope.myFile;
            $('.md-subhead').html('No Image');
        } else {
            if ($scope.pp.outlet_info == undefined) {
                angular.extend($scope.pp, {outlet_info: {image_url: undefined}});
            } else {
                angular.extend($scope.pp.outlet_info, {image_url: undefined});
            }
        }
    }
    
    if ($scope.path == '/outlets') {
        $scope.sortData = function(sorting) {
            console.log(sorting);
        }
        
        if (items == undefined) {
            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
            Projects.curlPost(Projects.zUrl, 'api/outlets/index', dtAcc).then(function(msg) {
                var line = msg.data;
                if(msg.status == 200)
                {
                    $scope.listdata = line.data;
                    $scope.listcount = line.data.length;
                }
            })
        }
        
        if (opts != undefined) {
            if (items.error == undefined) {
                // User Data
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/user/index', dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        var log = [];
                        $x = 0;
                        angular.forEach(line.data, function(val) {
                            log[$x] = angular.extend(val, {selected: false});
                            $x++;
                        })
                        
                        $scope.userData = log;
                    }
                })
                
                // Product Data
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/products/index', dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        var log = [];
                        $x = 0;
                        angular.forEach(line.data, function(val) {
                            log[$x] = angular.extend(val, {selected: false});
                            $x++;
                        })
                        
                        $scope.prodData = log;
                    }
                })
            } else {
                $scope.userData = items.user_data;
                $scope.prodData = items.product_data;
            }
        }
        
        // Add Data
        if (opts != undefined && opts.type == 0) {
            $scope.nm = opts.name;
            $scope.pp = items; console.log($scope.pp);
        }
        
        $scope.showAdd = function(ev, item) {
            if (Object.prototype.toString.call(item) === '[object Object]') {
                $scope.pp = angular.extend(item.outlet_data, {error: 1});
            } else {
                var d = new Date();
                $scope.pp = {is_active: 0, outlet_active_date: d};
            }
            
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'outletsCtrl',
                templateUrl: 'templates/v1/outlets/main/formdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 0, name: 'Add Outlet', action: 'api/outlets/create'}, items: $scope.pp},
                fullscreen: true
            })
        }
        
        // Edit Data
        if (opts != undefined && opts.type > 0) {
            $scope.nm = opts.name;
            if (items.error == undefined) {
                $scope.showData = function(items) {
                    if (items > 0) {
                        AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                        Projects.curlPost(Projects.zUrl, 'api/outlets/show/'+items, dtAcc).then(function(msg) {
                            var line = msg.data;
                            if(msg.status == 200)
                            {
                                var d = new Date(line.data.outlet_active_date);
                                angular.extend(line.data, {is_active: parseInt(line.data.is_active), outlet_active_date: d});
                                $scope.pp = line.data;
                            }
                        })
                    }            
                }
                $scope.showData(items);
            } else {
                $scope.pp = items;
            }
        }
        
        $scope.showEdit = function(ev, item) {
            var id = item;
            if (Object.prototype.toString.call(item) === '[object Object]') {
                $scope.pp = angular.extend(item.outlet_data, {error: 1});
                id = $scope.pp.outlet_id;
                item = $scope.pp;
            }
            
            $scope.Dialog = $mdDialog;
            $scope.Dialog.show({
                controller: 'outletsCtrl',
                templateUrl: 'templates/v1/outlets/main/formdata.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                locals: {opts: {type: 1, name: 'Edit Outlet', action: 'api/outlets/update/'+id}, items: item},
                fullscreen: true
            })
        }
        
        // Save & Update Data
        $scope.proceed = function(pp) {
            if (opts != undefined && opts.action != '') {
                var files = $scope.myFile; console.log(pp);
                var selUser = getID($filter('filter')($scope.userData, {selected: true}), 'user_id');
                var selProd = getID($filter('filter')($scope.prodData, {selected: true}), 'product_id');
                
                (pp.outlet_info == undefined) ? angular.extend(pp, {outlet_info: {empty: ''}}) : '';
                if (files != undefined) {
                    angular.extend(pp.outlet_info, {files: files});
                }
                
                var d = new Date(pp.outlet_active_date);
                var xmonth = parseFloat(d.getMonth())+1;
                var xdate = d.getFullYear()+'-'+xmonth+'-'+d.getDate();
                
                angular.extend(pp, {outlet_active_date: xdate, user_data: $scope.userData, user_id: selUser, product_data: $scope.prodData, product_id: selProd});
                angular.extend(dtAcc, {outlet_data: pp, outlet_users_data: {user_id: selUser}, outlet_products_data: {product_id: selProd}});
                console.log(dtAcc);
                
                switch(opts.type) {
                    case 0: var popup = $scope.showAdd; break;
                    case 1: var popup = $scope.showEdit; break;
                }
                
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlMulti(Projects.zUrl, opts.action, dtAcc, popup).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }
        }
        
        // Delete
        $scope.delID = function(id) {
            if (id > 0) {
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/outlets/delete/'+id, dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }            
        }
        
        // Pro Action
        $scope.proAct = function(id, act) {
            if (id > 0) {
                AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
                Projects.curlPost(Projects.zUrl, 'api/outlets/activator/'+id+'/'+act, dtAcc).then(function(msg) {
                    var line = msg.data;
                    if(msg.status == 200)
                    {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.body))
                                .clickOutsideToClose(true)
                                .title('Message')
                                .textContent(line.success.message)
                                .ariaLabel('Success Message')
                                .ok('OK')
                                .targetEvent()
                        );
                        $route.reload();
                    }
                })
            }            
        }
    }
    
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    
}])

// Settings
.controller('settingsCtrl', ['$scope', '$mdSidenav', '$route', '$location', 'Projects', 'AuthenticationService', 'sessionService',
                             '$mdMedia', '$mdDialog', 'opts', 'items', '$cordovaImagePicker', '$cordovaFileTransfer', 'deviceDetector',
                             function($scope, $mdSidenav, $route, $location, Projects, AuthenticationService, sessionService, $mdMedia, $mdDialog, opts, items,
                                      $cordovaImagePicker, $cordovaFileTransfer, deviceDetector) {
    
    $scope.dvdetect = deviceDetector;
    console.log($scope.dvdetect);
    
    $scope.pic = Projects.zImg;
    $scope.path = $location.path();
    $scope.tlmenu = 'Settings';
    $scope.mnmenu = {url1: 'templates/v1/menu.html', url2: 'templates/v1/settings/menu.html', urlsd: 'templates/v1/sidebar/menu.html'};
    
    $scope.openSide = function(val) {
        $mdSidenav(val).toggle()
    }
    
    var dtAcc = JSON.parse(sessionService.get('account'));
    //angular.extend(dtAcc, {menu_id: 1})
    $scope.accLog = dtAcc;
    
    $scope.delPic = function() {
        var files = $scope.myFile;
        
        if (files != undefined) {
            delete $scope.myFile;
            $('.md-subhead').html('No Image');
        } else {
            if ($scope.pp.outlet_info == undefined) {
                angular.extend($scope.pp, {user_info: {image_url: undefined}});
            } else {
                angular.extend($scope.pp.user_info, {image_url: undefined});
            }
        }
    }
    
    if ($scope.path == '/settings') {
        $scope.urlx = 'api/user/update'+dtAcc.user_id;
        $scope.pp = dtAcc;
    }
    
    // Save & Update Data
    $scope.proceed = function(pp, urlx) {
        if (urlx != '') {
            var files = $scope.myFile;
            
            if ($scope.path == '/settings') {
                var files = $scope.myFile;
                
                (pp.user_info == undefined) ? angular.extend(pp, {user_info: {empty: ''}}) : '';
                if (files != undefined) {
                    angular.extend(pp, {user_info: {files: files}});
                }
                
                angular.extend(dtAcc, {user_data: pp});
            }
            
            console.log(dtAcc);
            AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
            Projects.curlMulti(Projects.zUrl, urlx, dtAcc).then(function(msg) {
                var line = msg.data;
                if(msg.status == 200)
                {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.body))
                            .clickOutsideToClose(true)
                            .title('Message')
                            .textContent(line.success.message)
                            .ariaLabel('Success Message')
                            .ok('OK')
                            .targetEvent()
                    );
                    $route.reload();
                }
            })
        }
    }
    
    $scope.imagePic = function() {
        var options = {
            maximumImagesCount: 10,
            width: 800,
            height: 800,
            quality: 80
        };
    
        $cordovaImagePicker.getPictures(options)
        .then(function (results) {
            for (var i = 0; i < results.length; i++) {
                console.log('Image URI: ' + results[i]);
            }
        }, function(error) {
            // error getting photos
        });
    }
    
    $scope.fileDownload = function() {
        // File for download
        var url = "http://www.gajotres.net/wp-content/uploads/2015/04/logo_radni.png";
         
        // File name only
        var filename = url.split("/").pop();
         
        // Save location
        var targetPath = cordova.file.externalRootDirectory + filename;
         
        $cordovaFileTransfer.download(url, targetPath, {}, true).then(function (result) {
            console.log('Success');
        }, function (error) {
            console.log('Error');
        }, function (progress) {
            // PROGRESS HANDLING GOES HERE
        });
    }
   
    $scope.fileUpload = function() {
        // Destination URL 
        var url = "http://example.gajotres.net/upload/upload.php";
         
        //File for Upload
        var targetPath = cordova.file.externalRootDirectory + "logo_radni.png";
         
        // File name only
        var filename = targetPath.split("/").pop();
         
        var options = {
             fileKey: "file",
             fileName: filename,
             chunkedMode: false,
             mimeType: "image/jpg",
         params : {'directory':'upload', 'fileName':filename} // directory represents remote directory,  fileName represents final remote file name
         };
              
         $cordovaFileTransfer.upload(url, targetPath, options).then(function (result) {
             console.log("SUCCESS: " + JSON.stringify(result.response));
         }, function (err) {
             console.log("ERROR: " + JSON.stringify(err));
         }, function (progress) {
             // PROGRESS HANDLING GOES HERE
         });
    }
    
    //AuthenticationService.SetCredentials(Projects.zDoo, Projects.zKoo);
    //Projects.curlPost(Projects.zUrl, 'api/settings/index', dtAcc).then(function(msg) {
    //    var line = msg.data;
    //    if(msg.status == 200)
    //    {
    //        $scope.listdata = line.data;
    //        $scope.listcount = line.data.length;
    //    }
    //})
    
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    
}])

