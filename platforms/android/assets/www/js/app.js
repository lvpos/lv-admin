angular.module('accApp', ['ngRoute', 'ngSanitize', 'ngResource', 'ngAnimate', 'ngCordova', 
                          'ng.deviceDetector', 'angular-loading-bar', 'ngMaterial', 'chart.js', 'ui.bootstrap', 'daterangepicker', 
                          'dd.directives', 'ff.filters', 'ss.services',
                          'login.ctrl'])

.run(['$rootScope', '$location', 'sessionService', function($rootScope, $location, sessionService) {
    
    angular.element(document).ready(function() {
        if (window.cordova) {
            // Running in Device
            document.addEventListener("deviceready", function() {
                // console.log("Deviceready event has fired, bootstrapping AngularJS.");
                wd = $('body').width();
                if(window.isTablet || wd >= 400) {
                    screen.unlockOrientation();
                }else{
                    screen.lockOrientation('portrait');
                }
            },false);
        } else {
            // Running in Browser
            // console.log("Running in browser, bootstrapping AngularJS now.");
        }
    })
    
    /* Permission Route */
    
    var routeDash = ['/dashboard'];
    $rootScope.$on('$locationChangeStart', function(event, next, current) {
        /*if ($location.path() !== '/login') {
            var connected = sessionService.checklog();
        
            connected.then(function(msg) {
                if(msg.data.status > 0)
                {
                    if($location.path() == '/login')
                    {
                        $location.path(routeDash);
                    }
                } else {
                    if($location.path() !== '/forgot')
                    {
                        $location.path('login');
                    }
                }
            })
        }*/
        
        console.log($location.path());
        if (sessionService.get('uid_acc') == null) {
            var x = 0;
            switch ($location.path()) {
                case '/forgot-password': x = 1; break;
                case '/create-account': x = 1; break;
            }
            
            if(x == 0)
            {
                $location.path('login');
            }
        } else {
            if($location.path() == '/login')
            {
                $location.path(routeDash);
            }
        }
    });
    
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        $rootScope.title = current.$$route.title; console.log(current.$$route.title);
    });
}])

.config(['$routeProvider', '$locationProvider', 'cfpLoadingBarProvider', 'ChartJsProvider', '$mdGestureProvider', '$mdDateLocaleProvider',
         function($routeProvider, $locationProvider, cfpLoadingBarProvider, ChartJsProvider, $mdGestureProvider, $mdDateLocaleProvider) {
    $mdGestureProvider.skipClickHijack();
    $mdDateLocaleProvider.formatDate = function(date) {
        return moment(date).format('YYYY-MM-DD');
    };
    cfpLoadingBarProvider.spinnerTemplate = '<div class="bg-mask"></div><div class="box-popup"><div class="loading"></div></div>';
    
    // Configure all charts
    ChartJsProvider.setOptions({
        colors: ['#97BBCD', '#DCDCDC', '#F7464A', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360']
    });
    // Configure all doughnut charts
    ChartJsProvider.setOptions('doughnut', {
        cutoutPercentage: 60
    });
    
    $routeProvider
        .when("/login", {
            title: 'Login Admin',
            templateUrl : 'templates/v1/login.html',
            controller: 'loginCtrl'
        })
        .when("/create-account", {
            title: 'Created Account',
            templateUrl : 'templates/v1/register.html',
            controller: 'loginCtrl'
        })
        .when("/forgot-password", {
            title: 'Forgot Password',
            templateUrl : 'templates/v1/forgot.html',
            controller: 'loginCtrl'
        })
        
        .when("/dashboard", {
            title: 'Dashboard',
            templateUrl : 'templates/v1/dashboard.html',
            controller: 'dashCtrl'
        })
        
        .when("/reports", {
            title: 'Reports',
            templateUrl : 'templates/v1/reports/main/reports.html',
            controller: 'reportsCtrl',
            resolve: {
                opts: function() {},
                items: function() {}
            }
        })
        .when("/reports/item-sales", {
            title: 'Reports / Item Sales',
            templateUrl : 'templates/v1/reports/main/reports.html',
            controller: 'reportsCtrl',
            resolve: {
                opts: function() {},
                items: function() {}
            }
        })
        .when("/reports/category-sales", {
            title: 'Reports / Category Sales',
            templateUrl : 'templates/v1/reports/main/reports.html',
            controller: 'reportsCtrl',
            resolve: {
                opts: function() {},
                items: function() {}
            }
        })
        .when("/reports/transaction", {
            title: 'Reports / Transaction',
            templateUrl : 'templates/v1/reports/main/reports.html',
            controller: 'reportsCtrl',
            resolve: {
                opts: function() {},
                items: function() {}
            }
        })
        
        .when("/products", {
            title: 'Products',
            templateUrl : 'templates/v1/products/main/products.html',
            controller: 'productsCtrl',
            resolve: {
                opts: function() {},
                items: function() {}
            }
        })
        .when("/products/categories", {
            title: 'Products / Categories',
            templateUrl : 'templates/v1/products/categories/categories.html',
            controller: 'categoriesCtrl',
            resolve: {
                opts: function() {},
                items: function() {}
            }
        })
        .when("/products/packages", {
            title: 'Products / Packages',
            templateUrl : 'templates/v1/products/packages/packages.html',
            controller: 'packagesCtrl',
            resolve: {
                opts: function() {},
                items: function() {}
            }
        })
        .when("/products/promotions", {
            title: 'Products / Promotions',
            templateUrl : 'templates/v1/products/promotions/promotions.html',
            controller: 'promotionsCtrl',
            resolve: {
                opts: function() {},
                items: function() {}
            }
        })
        
        .when("/inventories/invent-in", {
            title: 'Inventory In',
            templateUrl : 'templates/v1/inventories/invent-in/invent-in.html',
            controller: 'inventinCtrl',
            resolve: {
                opts: function() {},
                items: function() {}
            }
        })
        .when("/inventories/invent-out", {
            title: 'Inventory Out',
            templateUrl : 'templates/v1/inventories/invent-out/invent-out.html',
            controller: 'inventoutCtrl',
            resolve: {
                opts: function() {},
                items: function() {}
            }
        })
        .when("/inventories/invent-move", {
            title: 'Inventory Move',
            templateUrl : 'templates/v1/inventories/invent-move/invent-move.html',
            controller: 'inventmoveCtrl',
            resolve: {
                opts: function() {},
                items: function() {}
            }
        })
        .when("/inventories/locations", {
            title: 'Inventory Locations',
            templateUrl : 'templates/v1/inventories/locations/locations.html',
            controller: 'inventlocsCtrl',
            resolve: {
                opts: function() {},
                items: function() {}
            }
        })
        
        .when("/customers", {
            title: 'Customers',
            templateUrl : 'templates/v1/customers/main/customers.html',
            controller: 'customersCtrl',
            resolve: {
                opts: function() {},
                items: function() {}
            }
        })
        
        .when("/user", {
            title: 'Users',
            templateUrl : 'templates/v1/user/main/user.html',
            controller: 'userCtrl',
            resolve: {
                opts: function() {},
                items: function() {}
            }
        })
        .when("/user/roles", {
            title: 'Users / Roles',
            templateUrl : 'templates/v1/user/roles/roles.html',
            controller: 'rolesCtrl',
            resolve: {
                opts: function() {},
                items: function() {}
            }
        })
        
        .when("/outlets", {
            title: 'Outlets',
            templateUrl : 'templates/v1/outlets/main/outlets.html',
            controller: 'outletsCtrl',
            resolve: {
                opts: function() {},
                items: function() {}
            }
        })
        
        .when("/settings", {
            title: 'Settings',
            templateUrl : 'templates/v1/settings/account.html',
            controller: 'settingsCtrl',
            resolve: {
                opts: function() {},
                items: function() {}
            }
        })
        
        .otherwise({
            redirectTo: '/login'
        });
}])

//.controller('AlertDemoCtrl', function ($scope) {
//    $scope.alerts = [
//        { type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.' },
//        { type: 'success', msg: 'Well done! You successfully read this important alert message.' }
//    ];
//  
//    $scope.addAlert = function() {
//        $scope.alerts.push({msg: 'Another alert!'});
//    };
//  
//    $scope.closeAlert = function(index) {
//        $scope.alerts.splice(index, 1);
//    };
//})

//angular.element(document).ready(function() {
//    angular.bootstrap(document, ['accApp']);
//});