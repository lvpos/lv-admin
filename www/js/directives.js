
angular.module('dd.directives', [])

.directive('hidScanner', ['$rootScope', function($rootScope) {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
	    var chars = [];
	    var pressed = false;
	    var hidScanner = function(e) {
		var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
		if (e.which >= 42 && e.which <= 122) {
		    chars.push(String.fromCharCode(charCode));
		}
		//console.log(e.which + ":" + chars.join("|"));
		
		if (pressed == false) {
		    var wait = $(this).data('wait');
		    if (wait) {
			clearTimeout(wait);
		    }
		    
		    wait = setTimeout(function() {
				//if (chars.length >= 10) {
				    var barcode = chars.join("");
				    $rootScope.$broadcast("hidScanner::scanned", {barcode: barcode});
				//}
				chars = [];
				pressed = false;
			    }, 200);
		    
		    $(this).data('wait', wait);
		    return false;
		}
		pressed = true;
	    }
	    document.addEventListener('keypress', hidScanner, false);
	    
	    $rootScope.$on("hidScanner::scanned", function(event, args) {
		if (scope.hidScan == undefined) {
		    document.removeEventListener('keypress', hidScanner, false);
		} else {
		    scope.addPack(args.barcode);
		    $rootScope.$apply();
		}
	    })
        }
    }
}])

.directive('selOutlet', ['$interval', function($interval) {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
	    $(elem).click(function() {
		$('.de-outlet').html($(this).html());
	    });
        }
    }
}])

.directive('fileModel', ['$parse', function($parse) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            elem.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, elem[0].files[0]);
                });
		
		$('.md-subhead').html(elem[0].value);
		$('md-card-title-media').html('<div class="md-media-md card-media"></div>');
            });
        }
    };
}])

.directive('checkProd', ['$parse', function($parse) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
	    setTimeout(function() {
		angular.forEach(scope.$parent.prodData, function(v1) {
		    var ss = 0;
		    if (scope.$parent.pp.product_id == undefined) {
			angular.forEach(scope.$parent.pp.products, function(v2) {
			    if (v1.product_id == v2.product_id) {
				ss = 1;
			    }
			})
		    } else {
			angular.forEach(scope.$parent.pp.product_id, function(v2) {
			    if (v1.product_id == v2) {
				ss = 1;
			    }
			})
		    }
		    
		    if (ss == 1) {
			angular.extend(v1, {selected: true});
		    }
		});
	    }, 500)
        }
    };
}])

.directive('checkCat', ['$parse', function($parse) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
	    setTimeout(function() {
		angular.forEach(scope.$parent.catData, function(v1) {
		    var ss = 0;
		    if (scope.$parent.pp.category_id == undefined) {
			angular.forEach(scope.$parent.pp.categories, function(v2) {
			    if (v1.category_id == v2.category_id) {
				ss = 1;
			    }
			})
		    } else {
			angular.forEach(scope.$parent.pp.category_id, function(v2) {
			    if (v1.category_id == v2) {
				ss = 1;
			    }
			})
		    }
		    
		    if (ss == 1) {
			angular.extend(v1, {selected: true});
		    }
		});
	    }, 500)
        }
    };
}])

.directive('checkUser', ['$parse', function($parse) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
	    setTimeout(function() {
		angular.forEach(scope.$parent.userData, function(v1) {
		    var ss = 0;
		    if (scope.$parent.pp.user_id == undefined) {
			angular.forEach(scope.$parent.pp.users, function(v2) {
			    if (v1.user_id == v2.user_id) {
				ss = 1;
			    }
			})
		    } else {
			angular.forEach(scope.$parent.pp.user_id, function(v2) {
			    if (v1.user_id == v2) {
				ss = 1;
			    }
			})
		    }
		    
		    if (ss == 1) {
			angular.extend(v1, {selected: true});
		    }
		});
	    }, 500)
        }
    };
}])

.directive('checkOutlet', ['$parse', function($parse) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
	    setTimeout(function() {
		angular.forEach(scope.$parent.outletData, function(v1) {
		    var ss = 0;
		    if (scope.$parent.pp.outlet_id == undefined) {
			angular.forEach(scope.$parent.pp.outlets, function(v2) {
			    if (v1.outlet_id == v2.outlet_id) {
				ss = 1;
			    }
			})
		    } else {
			angular.forEach(scope.$parent.pp.outlet_id, function(v2) {
			    if (v1.outlet_id == v2) {
				ss = 1;
			    }
			})
		    }
		    
		    if (ss == 1) {
			angular.extend(v1, {selected: true});
		    }
		});
	    }, 500)
        }
    };
}])

.directive('searchType', function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
	    $(elem).on('keydown', function(ev) {
		ev.stopPropagation();
	    });
        }
    }
})

//$scope.checkProd = function(id) {
//    if ($scope.pp != undefined) {
//	var sel = '';
//	
//	if (items.error == undefined) { console.log(1);
//	    angular.forEach($scope.pp.products, function(values){
//		if (values.product_id == id) {
//		    sel = true;
//		}   
//	    })
//	} else { console.log(2);
//	    angular.forEach($scope.pp.product_id, function(val){
//		if (val == id) {
//		    sel = true;
//		}   
//	    })
//	}
//	
//	return sel;
//    }
//}