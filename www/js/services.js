
angular.module('ss.services', [])

.factory('Page', function() {
    var title = 'default';
    return {
	title: function() { return title; },
	setTitle: function(newTitle) { title = newTitle }
    };
})

.factory('AuthenticationService', ['$http', '$rootScope', 'Base64', function($http, $rootScope, Base64) {
    var service = {};
    
    service.SetCredentials = function(username, password) {
        var authdata = Base64.encode(username + ':' + password);
	
	//$http.defaults.headers.common = {"Access-Control-Request-Headers": "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method"};
    	//$http.defaults.headers.common = {"Access-Control-Allow-Method": "GET, POST, OPTIONS, PUT, DELETE"};
        $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
    };

    service.ClearCredentials = function () {
        $rootScope.globals = {};
        $http.defaults.headers.common.Authorization = 'Basic ';
    };

    return service;
}])

.factory('formDataObject', function() {
    return function(data) {
        var fd = new FormData();
        angular.forEach(data, function(value, key) {
	    if(Object.prototype.toString.call(value) === '[object Object]' || Object.prototype.toString.call(value) === '[object Array]')
            {
		angular.forEach(value, function(v1, k1) {
		    fd.append(key+"["+k1+"]", v1);
		    if(Object.prototype.toString.call(v1) === '[object Object]' || Object.prototype.toString.call(v1) === '[object Array]')
		    {
			angular.forEach(v1, function(v2, k2) {
			    fd.append(key+"["+k1+"]["+k2+"]", v2);
			});
		    }
		});
	    } else {
		fd.append(key, value);
	    }
        });
        return fd;
    };
})

.factory('Projects', ['$http', '$rootScope', '$location', '$mdDialog', 'formDataObject', function($http, $rootScope, $location, $mdDialog, formDataObject) {
    return {
    	curlGet: function(weburl, uri, xdata) {
	    return $http.get(weburl+uri, xdata);
    	},
    	curlPost: function(weburl, uri, xdata, ev) {
	    return $http.post(weburl+uri, xdata)
	    .success(function(data, status, headers, config) {
		//console.log(data);
	    })
	    .error(function(data, status, headers, config) {
		console.log(data);
		if (data != undefined && data.error.message != undefined) {
		    $mdDialog.hide();
		    var confirm = $mdDialog.confirm()
				    .title('Message')
				    .htmlContent(data.error.message)
				    .ariaLabel('Alert')
				    .targetEvent('')
				    .ok('OK')
				    .cancel('Cancel');
		    $mdDialog.show(confirm).then(function() {
			if (ev != undefined) {
			    ev(null, xdata);
			}
		    });
		}
		
		if (data != undefined && data.error.log != undefined && data.error.log > 0) {
		    localStorage.removeItem('uid_acc');
		    localStorage.removeItem('account');
		    $location.path('login');
		}
	    });
    	},
	curlMulti: function(weburl, uri, xdata, ev) {
	    return $http.post(weburl+uri, formDataObject(xdata), {
		transformRequest: angular.identity,
		headers: {'Content-Type': undefined}
	    })
	    .success(function(data, status, headers, config) {
		//console.log(data);
	    })
	    .error(function(data, status, headers, config) {
		console.log(data);
		if (data != undefined && data.error.message != undefined) {
		    $mdDialog.hide();
		    var confirm = $mdDialog.confirm()
				    .title('Message')
				    .htmlContent(data.error.message)
				    .ariaLabel('Alert')
				    .targetEvent('')
				    .ok('OK')
				    .cancel('Cancel');
		    $mdDialog.show(confirm).then(function() {
			if (ev != undefined) {
			    ev(null, xdata);
			}
		    });
		}
		
		if (data != undefined && data.error.log != undefined && data.error.log > 0) {
		    localStorage.removeItem('uid_acc');
		    localStorage.removeItem('account');
		    $location.path('login');
		}
	    });
    	},
	//zUrl: 'http://lvpos.sambelpedes.com/webcode/',
	zUrl: 'http://localhost:8081/project/aplikasi/lvpos/webcode/',
	zImg: 'http://lvpos.sambelpedes.com/webcode/storage/images/',
	zDoo: '',
	zKoo: '',
	ss: {user_id: '1', user_role_id: '1', menu_id: '1', session_key: 'cfY4DblviObco7MZp5or13Z0z8dwa7DBPhIDTsktc3NmFrsebEl9Iux1CMwC', ignoreLoadingBar: true}
    }
}])

.factory('sessionService', ['Projects', 'AuthenticationService', function(Projects, AuthenticationService) {
    return {
	set: function(key, value) {
	    return localStorage.setItem(key, value);
	},
	get: function(key) {
	    return localStorage.getItem(key);
	},
	destroy: function(key) {
	    return localStorage.removeItem(key);
	},
	checklog: function() {
	    //AuthenticationService.SetCredentials(zDoor, zKey);
	    //return Projects.curlGet(urlAPI, 'agent/checksession')
	}
    }
}])

.factory('nmFormat', function() {
    return function(input, type) {
	input = input*1;
	if(isNaN(input))return"";
	var str = new String(input);
	var result = "", len = str.length;
	for(var i=len-1;i>=0;i--)
	{
	    if((i+1)%3 == 0 && i+1!=len)result+=".";
	    result+=str.charAt(len-1-i);
	}
	return result;
    }
})

.factory('getID', function() {
    return function(dt, field) {
	var log = []; $x = 0;
        angular.forEach(dt, function(val) {
            log[$x] = val[field];
            $x++;
        })
        return log;
    }
})

.factory('Base64', function() {
    var keyStr = 'ABCDEFGHIJKLMNOP' +
		'QRSTUVWXYZabcdef' +
		'ghijklmnopqrstuv' +
		'wxyz0123456789+/' +
		'=';
    return {
        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
	    
            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
		
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
		
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
		
                output = output +
                        keyStr.charAt(enc1) +
                        keyStr.charAt(enc2) +
                        keyStr.charAt(enc3) +
                        keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);
	    
            return output;
        },
	
        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
	    
            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                alert("There were invalid base64 characters in the input text.\n" +
                        "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                        "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
	    
            do {
                enc1 = keyStr.indexOf(input.charAt(i++));
                enc2 = keyStr.indexOf(input.charAt(i++));
                enc3 = keyStr.indexOf(input.charAt(i++));
                enc4 = keyStr.indexOf(input.charAt(i++));
		
                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;
		
                output = output + String.fromCharCode(chr1);
		
                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }
		
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
		
            } while (i < input.length);
	    
            return output;
        }
    };
})

.factory('capitalize', function() {
    return function(input) {
	return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
})

//.service('goBackMany', function($ionicHistory) {
//    return function(depth){
//	// get the right history stack based on the current view
//	var historyId = $ionicHistory.currentHistoryId();
//	var history = $ionicHistory.viewHistory().histories[historyId];
//	// set the view 'depth' back in the stack as the back view
//	var targetViewIndex = history.stack.length - 1 - depth;
//	$ionicHistory.backView(history.stack[targetViewIndex]);
//	// navigate to it
//	$ionicHistory.goBack();
//    }
//})